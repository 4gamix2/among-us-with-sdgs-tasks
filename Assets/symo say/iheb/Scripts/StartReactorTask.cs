using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartReactorTask : MonoBehaviour
{


    [SerializeField] private Color activeColor = Color.yellow;

    [SerializeField] private float showColorTime = 0.5f;
    [SerializeField] private List<StartReactorButton> buttonList = new List<StartReactorButton>();
    [SerializeField] private List<Image> animatedImagesList = new List<Image>();
    [SerializeField] private List<Image> taskProgressesList = new List<Image>();

    private int currentSequenceIndex = 1;

    private int currentStep;

    private List<int> generatedSequence;

    private bool IsSequenceGenerated = false;

    public Slider progressBar;



    private void OnEnable()
    {
        if (IsSequenceGenerated == false)
        {
            generatedSequence = new List<int>();

            List<int> uniqueImageSequence = new List<int>();
            for (int i = 0; i < animatedImagesList.Count; i++)
            {
                uniqueImageSequence.Add(i);
            }

            for (int i = 0; i < animatedImagesList.Count; i++)
            {
                int selectedIndex = uniqueImageSequence[Random.Range(0, uniqueImageSequence.Count)];
                uniqueImageSequence.RemoveAt(0);
                generatedSequence.Add(selectedIndex);
            }

            for (int i = 0; i < buttonList.Count; i++)
            {
                buttonList[i].Initialize(i, this);
            }

            foreach (Image img in taskProgressesList)
            {
                img.color = Color.white;
            }

            currentSequenceIndex = 1;
        }

        StartCoroutine(PlaySequence());
    }

    private void ToggleButtons(bool isActive)
    {
        foreach (StartReactorButton button in buttonList)
        {
            button.Toggle(isActive);
        }
    }

    private IEnumerator PlaySequence()
    {
        ToggleButtons(false);

        for (int i = 0; i < currentSequenceIndex; i++)
        {
            animatedImagesList[generatedSequence[i]].color = activeColor;
            yield return new WaitForSeconds(showColorTime);
            animatedImagesList[generatedSequence[i]].color = Color.white;
        }
        ToggleButtons(true);
    }

    private IEnumerator ShowError()
    {
        ToggleButtons(false);


        for (int i = 0; i < 3; i++)
        {
            foreach (Image img in animatedImagesList)
            {
                img.color = Color.red;
            }
            yield return new WaitForSeconds(0.5f);
            foreach (Image img in animatedImagesList)
            {
                img.color = Color.white;
            }

            StartCoroutine(PlaySequence());

        }
    }

    public void OnButtonPressed(int index)
    {
        if (generatedSequence[currentStep] == index)
        {
            currentStep++;
            if (currentStep >= currentSequenceIndex)
            {
                taskProgressesList[currentSequenceIndex - 1].color = Color.green;
                currentSequenceIndex++;
                currentStep = 0;

                if (currentSequenceIndex <= taskProgressesList.Count)
                {
                    StartCoroutine(PlaySequence());

                }
            }
        }
        else {
            currentStep = 0;
            StartCoroutine(ShowError());
        }

        if (currentSequenceIndex > taskProgressesList.Count)
        {
            IsSequenceGenerated = false;
            gameObject.SetActive(false);
            progressBar.value = 1f;
            Destroy(this, 1f);
        }
    }
}