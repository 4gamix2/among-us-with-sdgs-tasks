using System.Data;
using UnityEngine;

using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
//using Vignette = UnityEngine.Rendering.PostProcessing.Vignette;

public class DesactiverLumiere : MonoBehaviour
{
    public GameObject panel;
    public Light targetLight; // R�f�rence � la lumi�re que vous souhaitez d�sactiver
    public GameObject buttonQuiz;
    public GameObject Interaction;
    public GameObject buttonDesableLight;
    public GameObject mainLum;
    public new GameObject camera;  
    public AudioSource audioSource; // Source audio pour jouer le son
    public AudioClip soundEffect; // Effet sonore � jouer
    public AudioSource pcClick; // Source audio pour jouer le son
    public AudioClip PCclick; // Effet sonore � jouer
    
    
    public Volume postProcessVolume;
    
    //private Vignette MyVignette;
   
    [SerializeField]
    public FloatParameter lowIntensity = new FloatParameter(0f);
    [SerializeField]
    public FloatParameter highIntensity = new FloatParameter(1f);

    public MainMenu mainMenu ;
    
    private Light spotLight;

    public GameObject luminosit�Button;
    


    public void Start()
    {
        targetLight = GetComponent<Light>();
        mainMenu = FindObjectOfType<MainMenu>();
        panel.SetActive(false);
        Interaction.SetActive(false);
        buttonQuiz.SetActive(false);
        //postProcessVolume.profile.TryGet(out MyVignette);
        Color ambientColor = RenderSettings.ambientLight;
        

    }
    //private void Update()
    //{
        
        
    //    MyVignette.active = true;
    //}



    public void DesactiverLumieree()
    {
        if (mainMenu.PlayerRole == "Imposteur") 
        {
            luminosit�Button.SetActive(false);
            RenderSettings.ambientLight = new Color(0f, 0f, 0f , 0f);
            //spotLight.enabled = true;
            //MyVignette.intensity.SetValue(highIntensity);
            Interaction.SetActive(true);

            targetLight.enabled = false;
            

            Vector3 currentRotation = mainLum.transform.rotation.eulerAngles;
            currentRotation.z += 90f;
            mainLum.transform.rotation = Quaternion.Euler(currentRotation);

            audioSource.PlayOneShot(soundEffect);
        }
        else if (mainMenu.PlayerRole == "Gardien")
        {
            Interaction.SetActive(false);
        }
    }




    public void activerLumieree()
    {
        //if (mainMenu.PlayerRole == "Gardien") 
        //{

        luminosit�Button.SetActive(true);
        //RenderSettings.ambientLight = new Color(190f, 190f, 190f, 0f);
        //MyVignette.intensity.SetValue(lowIntensity);
        targetLight.enabled = true;
            panel.SetActive(false);
            Destroy(Interaction);
            buttonDesableLight.SetActive(false);
            buttonQuiz.SetActive(false);
            pcClick.PlayOneShot(PCclick);
        //}
    }
    public void afficherQuiz()
    {
        panel.SetActive(true);
    }
    public void Cancel()
    {
        panel.SetActive(false);
    }

}
