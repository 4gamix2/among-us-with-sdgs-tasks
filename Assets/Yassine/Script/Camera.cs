using Unity.Netcode;
using UnityEngine;

public class CameraFolloww : MonoBehaviour
{
    private Transform target; // R�f�rence au transform du joueur � suivre
    public float smoothSpeed = 0.125f; // Vitesse de d�placement de la cam�ra
    public Vector3 offset; // D�calage de la cam�ra par rapport au joueur
    public Vector3 rotationOffset; // Offset de rotation suppl�mentaire de la cam�ra



    private GameObject playerPrefab;
    public Transform spawnPoint; // Point de spawn du joueur

    private GameObject instantiatedPlayer; // R�f�rence � la clone du joueur cr��e

    

    void Start()
    {
        // �coute les �v�nements de connexion au r�seau
        NetworkManager.Singleton.OnClientConnectedCallback += HandleClientConnected;
        
    }

    
    public void StartHost()
    {
        // D�marrer en tant qu'h�te
        NetworkManager.Singleton.StartHost();

        // Instancier le joueur localement
        instantiatedPlayer = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);

        
    }


    public void StartClient()
    {
        // D�marrer en tant que client
        NetworkManager.Singleton.StartClient();

        // Laisser le serveur instancier le joueur, puis le r�cup�rer
        NetworkManager.Singleton.OnClientConnectedCallback += HandleClientConnected;
        instantiatedPlayer = Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
    }

    private void HandleClientConnected(ulong clientId)
    {
        // R�cup�re la r�f�rence au joueur cr�� sur le client connect�
        var networkObject = NetworkManager.Singleton.SpawnManager.GetPlayerNetworkObject(clientId);
        if (networkObject != null)
        {
            // Mettez � jour la r�f�rence target pour pointer vers la clone du joueur
            target = networkObject.transform;
        }
    }
    



    void FixedUpdate()
    {
        if (target != null)
        {
            // Calcule la position cible de la cam�ra en ajoutant le d�calage au joueur
            Vector3 desiredPosition = target.position + offset;

            // Interpole la position actuelle de la cam�ra vers la position cible de mani�re fluide
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

            // Applique l'offset de rotation suppl�mentaire � la rotation de la cam�ra
            Quaternion desiredRotation = Quaternion.Euler(rotationOffset);

            // Met � jour la position et la rotation de la cam�ra
            transform.position = smoothedPosition;
            transform.rotation = desiredRotation;

            // V�rifie si le joueur se d�place et active l'animation en cons�quence
            float moveSpeed = target.GetComponent<Rigidbody>().velocity.magnitude;
            bool isMoving = moveSpeed > 0.1f; // Vous pouvez ajuster cette valeur selon votre besoin
        }
    }
}
