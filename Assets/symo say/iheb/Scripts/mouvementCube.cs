using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public float rotationSpeed = 50f; // Vitesse de rotation
    public GameObject rotatingObject;
    void Update()
    {
        // Tourner le GameObject autour de l'axe Y
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }


    public void OnStopRotationButtonClick()
    {
        // V�rifier si le GameObject est valide
        if (rotatingObject != null)
        {
            // D�sactiver le script de rotation du GameObject
            RotateObject rotationScript = rotatingObject.GetComponent<RotateObject>();
            if (rotationScript != null)
            {
                rotationScript.enabled = false; // D�sactiver le script de rotation
            }
            else
            {
                Debug.LogWarning("Le GameObject ne poss�de pas le script de rotation.");
            }
        }
        else
        {
            Debug.LogWarning("La r�f�rence vers le GameObject est nulle. Assurez-vous de l'assigner dans l'�diteur Unity.");
        }
    }

    public void OnResumeRotationButtonClick()
    {
        // V�rifier si le GameObject est valide
        if (rotatingObject != null)
        {
            // Activer le script de rotation du GameObject
            RotateObject rotationScript = rotatingObject.GetComponent<RotateObject>();
            if (rotationScript != null)
            {
                rotationScript.enabled = true; // Activer le script de rotation
            }
            else
            {
                Debug.LogWarning("Le GameObject ne poss�de pas le script de rotation.");
            }
        }
        else
        {
            Debug.LogWarning("La r�f�rence vers le GameObject est nulle. Assurez-vous de l'assigner dans l'�diteur Unity.");
        }
    }
}