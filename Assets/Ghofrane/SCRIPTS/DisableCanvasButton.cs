using UnityEngine;
using UnityEngine.UI;

public class DisableCanvasButton : MonoBehaviour
{
    public DisableCanvas disableCanvasScript;

    private void Start()
    {
        Button button = GetComponent<Button>();
        if (button != null && disableCanvasScript != null)
        {
            button.onClick.AddListener(disableCanvasScript.DisableEntireCanvas);
        }
        else
        {
            Debug.LogWarning("Aucun bouton ou script de désactivation de Canvas assigné.");
        }
    }
}
