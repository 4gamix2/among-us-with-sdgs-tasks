using UnityEngine;
using UnityEngine.UI;

public class DetectPlayerProximity : MonoBehaviour
{
    public Button quizButton; // R�f�rence au bouton du quiz
    public MainMenu mainMenu;

    private void Start()
    {
        mainMenu = FindObjectOfType<MainMenu>();
    }

    void OnTriggerEnter(Collider other)
    {
        // V�rifie si le joueur entre en collision avec la sph�re
        if (other.CompareTag("Player") )//&& mainMenu.PlayerRole == "Gardien")
        {
            quizButton.gameObject.SetActive(true); // Active le bouton
         
        }
    }

    void OnTriggerExit(Collider other)
    {
        // V�rifie si le joueur quitte la collision avec la sph�re
        if (other.CompareTag("Player"))
        {
            quizButton.gameObject.SetActive(false); // D�sactive le bouton
            
        }
    }
}
