using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.Windows;


public class ProgressBarController : MonoBehaviour
{
    public Slider globalProgressBar;
    public Slider progressBar1;
    public Slider progressBar2;
    public Slider progressBar3;
    float lasttask1 = 0;
    float lasttask2 = 0;
    float lasttask3 = 0;
    public string tcode="";




    IEnumerator CreateH()
    {


        Debug.Log("hey  there");
        // Create a form object to send data to the server
        WWWForm form = new WWWForm();

        form.AddField("task1", "0");
        form.AddField("task2", "0");
        form.AddField("task3", "0");
        form.AddField("task4", "0");
        form.AddField("iduser", "1");


        UnityWebRequest www = UnityWebRequest.Post("http://localhost:9090/tasks/add", form);
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.Success)
        {

            tcode = (www.downloadHandler.text);
            tcode = tcode.Substring(1, tcode.Length - 2);
            Debug.Log("code name is: " + tcode);
            Debug.Log("task Add successfully!");
        }
        else
        {
            Debug.LogError(www.error);
        }
    }




    IEnumerator updateTasks(float t1, float t2,float t3)
    {


        Debug.Log("hey  there");
        // Create a form object to send data to the server
        WWWForm form = new WWWForm();

        form.AddField("task1", t1.ToString());
        form.AddField("task2", t2.ToString());
        form.AddField("task3", t3.ToString());
        form.AddField("task4", "0");
        form.AddField("iduser", "1");


        UnityWebRequest www = UnityWebRequest.Post("http://localhost:9090/tasks/update/"+ tcode, form);
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.Success)
        {

          
          
            Debug.Log("code name is: " + tcode);
            Debug.Log("task Add successfully!");
        }
        else
        {
            Debug.LogError(www.error);
        }

    }



    void Start()
    {
        StartCoroutine(CreateH());
        
    }  

    void Update()
    {
  
       
        if (globalProgressBar != null && progressBar1 != null && progressBar2 != null && progressBar3 !=null)
        {
          
           
            // Get the values of the two progress bars
            float progressValue1 = progressBar1.value;
            float progressValue2 = progressBar2.value;
            float progressValue3 = progressBar3.value;

            if ((progressBar1.value != lasttask1)||(progressBar2.value != lasttask2) || (progressBar3.value != lasttask3))
            {
                lasttask2 = progressBar2.value;
                lasttask1 = progressBar1.value;
                lasttask3 = progressBar3.value;
                Debug.Log("task 1 tbadill");
                StartCoroutine(updateTasks(lasttask1, lasttask2, lasttask3));

            }
           


            // Calculate the average of the two progress bars
            float globalProgressValue = (progressValue1 + progressValue2+ progressValue3) / 3f;

            // Update the global progress bar
            globalProgressBar.value = globalProgressValue;
        }
      

       




    }
}
