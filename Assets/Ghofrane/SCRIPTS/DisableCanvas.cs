using UnityEngine;

public class DisableCanvas : MonoBehaviour
{
    public void DisableEntireCanvas()
    {
        Canvas Canvas1 = GetComponentInChildren<Canvas>(); // R�cup�re le Canvas dans les enfants du GameObject courant
        if (Canvas1 != null)
        {
            Canvas1.gameObject.SetActive(false); // D�sactive le GameObject contenant le Canvas
        }
        else
        {
            Debug.LogWarning("Aucun Canvas trouv� dans les enfants du GameObject courant.");
        }
    }
}
