using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject objectToSpawn; // GameObject � instancier
    public float spawnInterval = 30f; // Intervalle entre chaque spawn en secondes
    public float spawnDuration = 30f; // Dur�e de vie de chaque spawn en secondes
    public Transform[] spawnPoints; // Positions de spawn

    private float nextSpawnTime; // Temps du prochain spawn

    void Start()
    {
        // Initialiser le temps du premier spawn
        nextSpawnTime = Time.time + spawnInterval;
    }

    void Update()
    {
        // V�rifier si le temps pour un nouveau spawn est �coul�
        if (Time.time >= nextSpawnTime)
        {
            // Choisir une position de spawn 
            Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];

            // Instancier un nouvel objet � la position de spawn
            GameObject spawnedObject = Instantiate(objectToSpawn, spawnPoint.position, Quaternion.identity);

            // D�finir la dur�e de vie de l'objet instanci�
            Destroy(spawnedObject, spawnDuration);

            // Mettre � jour le temps du prochain spawn
            nextSpawnTime = Time.time + spawnInterval;
        }
    }
}
