using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TextUpdater : MonoBehaviour
{
    public TextMeshProUGUI texteCanvas1;
    public TextMeshProUGUI texteCanvas2;
    private int codeAcces;  // Le code d'acc�s g�n�r� al�atoirement.
    public TextMeshProUGUI codeGenereText; // Ajoutez cette ligne

    private void Start()
    {
        // G�n�re un code d'acc�s al�atoire de 4 chiffres au d�marrage du jeu.
        GenererCodeAcces();
    }

    private void GenererCodeAcces()
    {
        // G�n�re un code d'acc�s al�atoire de 4 chiffres.
        codeAcces = Random.Range(1000, 10000);
        Debug.Log("Code d'acc�s g�n�r� : " + codeAcces);
        codeGenereText.text = codeAcces.ToString();
    }

    void Update() => texteCanvas2.text = texteCanvas1.text;
}

