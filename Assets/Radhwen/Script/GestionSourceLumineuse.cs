using UnityEngine;
using UnityEngine.UI;

public class GestionSourceLumineuse : MonoBehaviour
{
    // R�f�rence vers la source lumineuse
    public Light sourceLumineuse;

    private void Start()
    {
        // Assurez-vous que la source lumineuse est assign�e dans l'�diteur Unity
        if (sourceLumineuse == null)
        {
            Debug.LogError("Source lumineuse non assign�e.");
            return;
        }

        // Ajoutez une fonction � appeler lors du clic sur le premier bouton
        transform.Find("BoutonAllumer").GetComponent<Button>().onClick.AddListener(AllumerSource);

        // Ajoutez une fonction � appeler lors du clic sur le deuxi�me bouton
        transform.Find("BoutonEteindre").GetComponent<Button>().onClick.AddListener(EteindreSource);
    }

    private void AllumerSource()
    {
        // Activez la source lumineuse lorsque vous cliquez sur le bouton "Allumer"
        sourceLumineuse.enabled = true;
    }

    private void EteindreSource()
    {
        // D�sactivez la source lumineuse lorsque vous cliquez sur le bouton "Eteindre"
        sourceLumineuse.enabled = false;
    }
}
