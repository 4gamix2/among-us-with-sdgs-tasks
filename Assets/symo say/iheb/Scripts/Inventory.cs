using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class Inventory : MonoBehaviour
{
    [SerializeField] private List<ItemData> content = new List<ItemData>();

    [SerializeField] private GameObject inventoryPanel;

    [SerializeField] private Transform inventorySlotsParent;

    const int InventorySize = 24;

    [Header("Action Panel References")] [SerializeField]
    private GameObject actionPanel;

    [SerializeField] private GameObject dropItemButton;

    [SerializeField] private GameObject destroyItemButton;

    private ItemData itemCurrentlySelected;


    [SerializeField] private Sprite emptyslotVisual;

    [SerializeField] private Transform dropPoint;



    public static Inventory instance;

    private void Awake()
    {
        instance = this;
    }


    private void Start()
    {
        RefreshContent();

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventoryPanel.SetActive(!inventoryPanel.activeSelf);
        }
    }

    public void AddItem(ItemData item)
    {
        content.Add(item);
        RefreshContent();
    }

    public void CloseInventory()
    {
        inventoryPanel.SetActive(false);
    }

    private void RefreshContent()
    {
        //1111111111111111111
        for (int i = 0; i < inventorySlotsParent.childCount; i++)
        {
            Slot currentSlot = inventorySlotsParent.GetChild(i).GetComponent<Slot>();

            currentSlot.item = null;
            currentSlot.itemVisual.sprite = emptyslotVisual;

        }

        // 22222222222222222222
        for (int i = 0; i < content.Count; i++)
        {
            Slot currentSlot = inventorySlotsParent.GetChild(i).GetComponent<Slot>();

            currentSlot.item = content[i];
            currentSlot.itemVisual.sprite = content[i].visual;
        }
    }

    public bool IsFull()
    {
        return InventorySize == content.Count;
    }



    public void OpenActionPanel(ItemData item, Vector3 slotposition)
    {
        itemCurrentlySelected = item;

        if (item == null)
        {
            actionPanel.SetActive(false);
            return;
        }

        actionPanel.transform.position = slotposition;
        actionPanel.SetActive(true);


    }

    public void CloseActionPanel()
    {

        actionPanel.SetActive(false);
        itemCurrentlySelected = null;


    }

    public void DropActionButton()
    {
        GameObject instantiatedItem = Instantiate(itemCurrentlySelected.prefab);
        instantiatedItem.transform.position = dropPoint.position;
        content.Remove(itemCurrentlySelected);
        RefreshContent();
        CloseActionPanel();
        StartCoroutine(DeleteItemFromDatabase(itemCurrentlySelected.name));
    }

    public void DestroyActionButton()
    {
        content.Remove(itemCurrentlySelected);
        RefreshContent();
        CloseActionPanel();

    }


    IEnumerator DeleteItemFromDatabase(string itemId)
    {
        // Create a new UnityWebRequest to delete the item from the server
        using (UnityWebRequest www = UnityWebRequest.Delete($"http://localhost:3000/api/inventory/delete/{name}"))
        {
            // Send the request and wait for the response
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"Error while deleting: {www.result}, {www.error}");
            }
            else
            {
                Debug.Log($"Success: {www.downloadHandler.text}");
            }
        }




    }
}
