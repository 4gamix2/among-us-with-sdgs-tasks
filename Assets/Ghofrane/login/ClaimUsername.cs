using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ClaimUsername : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI username;
    // Start is called before the first frame update
     // Update is called once per frame 
    void Awake()
    {
        username.text = PlayerPrefs.GetString("name");
    }
}
