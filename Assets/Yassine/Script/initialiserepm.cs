using UnityEngine;

public class InitialPosition : MonoBehaviour
{
    public Vector3 initialPosition = new Vector3(-265.57f, 2.428f, -50.318f); // Position initiale de votre choix

    void Start()
    {
        // D�finir la position initiale du GameObject au d�but du jeu
        transform.position = initialPosition;
    }
}
