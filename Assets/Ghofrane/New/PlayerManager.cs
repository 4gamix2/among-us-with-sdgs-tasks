using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine.UI;


public class PlayerManager : NetworkBehaviour
{

    GridLayoutGroup gridLayoutGroup;
    [SerializeField] private GameObject prefpss;

    int lentlist = 0;
    int lentlist1 = 0;


    float timer = 0f;
    float timerInterval = 100f;

    private Dictionary<uint, PlayerHealth> players = new Dictionary<uint, PlayerHealth>();
    public NetworkVariable<bool> playevote = new NetworkVariable<bool>(true);

    public GameObject aa;
    void Start()
    {
       

        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        addviews();
    }



    /*
    public override void OnNetworkSpawn()
        {
            base.OnNetworkSpawn();
               gridLayoutGroup= GetComponent<GridLayoutGroup>();
               GameObject newElement = Instantiate(prefpss, gridLayoutGroup.transform);

        }*/

    void Update()
    {
        /*
        
        if (playevote.Value == false)
        {
            aa.SetActive(false);
        }
        else
        {
            aa.SetActive(true);
        }
        */
        /* if(IsServer){
           Debug.Log("i am server");
         }else if(IsClient){
           Debug.Log("i am client");

         }*/

    

            //  GameObject newElement = Instantiate(prefpss, gridLayoutGroup.transform);
            int numClients;
        if (IsServer)
        {


            numClients = NetworkManager.Singleton.ConnectedClientsList.Count;
            SendNumClientsToClientsServerRpc(numClients);
        }
        if (lentlist != lentlist1)
        {
            //    Debug.Log("Number of clients changed: " + lentlist);
            print("Number of clients changed2: " + lentlist1); // Show alert when number of clients changes
            lentlist1 = lentlist;

        }

        timer += Time.deltaTime;

        if (timer >= timerInterval)
        {
            // Reset the timer
            // Reset the timer
            timer = 0f;

            // Find all PlayerHealth components
            PlayerHealth[] playerHealthComponents = FindObjectsOfType<PlayerHealth>();

            // If there are no players, return
            if (playerHealthComponents.Length == 0)
            {
                Debug.LogWarning("No players found.");
                return;
            }

            // Find the minimal current health value
            float minHealth = float.MaxValue;
            int idd = 0;
            foreach (PlayerHealth playerHealth in playerHealthComponents)
            {
                if (playerHealth.currentHealth.Value < minHealth)
                {
                    print(playerHealth.NetworkObjectId);
                    idd = (int)playerHealth.NetworkObjectId;
                    minHealth = playerHealth.currentHealth.Value;
                }
            }
            PlayerPrefs.SetInt("idd", idd);
            // Print the minimal current health value
            //  Debug.Log("Minimal current health value: " + minHealth);
            addviews();
            playevote.Value = false;

        }



    }


    public void addviews()
    {

        foreach (Transform child in gridLayoutGroup.transform)
        {
            // Destroy each child object
            Destroy(child.gameObject);
        }


        //     Debug.Log("chansedd of clients: ");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

        foreach (GameObject player in players)
        {
            GameObject newElement = Instantiate(prefpss, gridLayoutGroup.transform);


            newElement.GetComponent<uiinfo>().insertinf(player.GetComponent<SC_FPSController>().nn());
            //  Debug.Log("Found player: " + player.name);

        }


        PlayerHealth[] playerHealthComponents = FindObjectsOfType<PlayerHealth>();

        // Add each player to the dictionary
        foreach (PlayerHealth playerHealth in playerHealthComponents)
        {
            //  players.Add((uint)playerHealth.NetworkObjectId, playerHealth);
            Debug.Log(playerHealth.NetworkObjectId + " - " + playerHealth.currentHealth.Value);
        }


    }


    [ServerRpc(RequireOwnership = false)]
    void SendNumClientsToClientsServerRpc(int numClients)
    {
        RpcReceiveNumClientsClientRpc(numClients);
    }

    [ClientRpc]
    void RpcReceiveNumClientsClientRpc(int numClients)
    {
        //     Debug.Log("Number of clients: " + numClients);
        if (numClients > lentlist)
        {
            //      Debug.Log("chansedd of clients: " + numClients);
            lentlist = numClients;
            addviews();
        }

    }
     public void DestroyChildrenByTag()
    {
        GameObject[] children = GameObject.FindGameObjectsWithTag("btndestroy");
        foreach (GameObject child in children)
        {
            Destroy(child);
        }
    }


}
