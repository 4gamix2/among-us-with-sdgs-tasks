using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class PlayerMovement2 : NetworkBehaviour
{

      public float speed = 5f;
    public override void OnNetworkSpawn()
    {
        if(!IsOwner){
                enabled=false;
                return;
        }
    }


    void Update()
    {
       
        // Get input from arrow keys
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calculate movement direction
        Vector3 movement = new Vector3(horizontalInput,0f , verticalInput) * speed * Time.deltaTime;

        // Apply movement to the player's position
        transform.position += movement;
    }
}


