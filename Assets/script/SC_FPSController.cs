using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Networking;





[System.Serializable]
public class NetworkStringVariable
{
    public NetworkVariable<string> value;

    public NetworkStringVariable(string initialValue)
    {
        value = new NetworkVariable<string>(initialValue);
    }
}



[RequireComponent(typeof(CharacterController))]

public class SC_FPSController : NetworkBehaviour
{
    // public NetworkVariable<string> namep = new NetworkVariable<string>("aa");

    public NetworkVariable<FixedString128Bytes> namep = new NetworkVariable<FixedString128Bytes>();
    // public NetworkVariable<uint> namep = new NetworkVariable<uint>(0);
    //  public NetworkStringVariable namep = new NetworkStringVariable("Player");



    //  public string namep="aabbb";
    public float walkingSpeed = 7.5f;
    public float runningSpeed = 11.5f;
    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;
    public Camera playerCamera;
    public float lookSpeed = 2.0f;
    public float lookXLimit = 45.0f;

    public float px=10;
    public float py=10;
    public float pz=10;

    //CharacterController characterController;
    Vector3 moveDirection = Vector3.zero;
    float rotationX = 0;

    [HideInInspector]
    public bool canMove = true;

    public GameObject minimapCamera;


    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        transform.position = new Vector3(px,py,pz);
        string aa = PlayerPrefs.GetString("name");
        namep.Value = new FixedString128Bytes("player : "+ GetComponent<NetworkObject>().NetworkObjectId);
        // namep.Value=(uint)GetComponent<NetworkObject>().NetworkObjectId;
    }

    void Start()
    {
        if (!IsOwner)
        {
            playerCamera.gameObject.SetActive(false);
            minimapCamera.SetActive(false);
            return;
        }
        Debug.Log(" --  " + (uint)GetComponent<NetworkObject>().NetworkObjectId);

       


        // Lock cursor
        //   Cursor.lockState = CursorLockMode.Locked;
        //   Cursor.visible = false;
    }

    void Update()
    {

        /*
          if (PlayerPrefs.GetInt("openv") == 5)
          {
             // PlayerPrefs.DeleteKey("opencv");
              SendButtonClickToServerRpc();

          }*/

    

    }

    public FixedString128Bytes nn()
    {
        return namep.Value;
    }

    [ServerRpc]
    void UpdatePlayerNameServerRpc(FixedString128Bytes newName)
    {
        namep.Value = newName; // Access the value field directly
    }

    public void ChangePlayerName(FixedString128Bytes newName)
    {
        if (!IsLocalPlayer)
            return;

        UpdatePlayerNameServerRpc(newName);
    }


    



}

