using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class gettaskess : MonoBehaviour
{
    // Start is called before the first frame update
    string tcode;
    GridLayoutGroup gridLayoutGroup;
    public TMP_Text name;
    [SerializeField] private GameObject prefpss;

    List<TaskInfo> taskList = new List<TaskInfo>();
    void Start()
    {
        name.text = PlayerPrefs.GetString("name");
        gridLayoutGroup = GetComponent<GridLayoutGroup>();
        StartCoroutine(getH());
    }

    // Update is called once per frame
    void Update()
    {
      
    }


    IEnumerator getH()
    {


        Debug.Log("hey  there");
        // Create a form object to send data to the server
        WWWForm form = new WWWForm();

     



        UnityWebRequest www = UnityWebRequest.Get("http://localhost:9090/tasks/byuser/1");
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.Success)
        {
            string json = www.downloadHandler.text;
            TaskInfoList taskInfoList = JsonUtility.FromJson<TaskInfoList>("{\"tasks\":" + json + "}");
            taskList = taskInfoList.tasks;
            Debug.Log(taskList);
            foreach (var task in taskList)
            {
                GameObject newElement = Instantiate(prefpss, gridLayoutGroup.transform);
                Debug.Log("ID: " + task.id);
               /* Debug.Log("ESM: " + task.esm);
                Debug.Log("Task 1: " + task.task1);
                Debug.Log("Task 2: " + task.task2);
                Debug.Log("Task 3: " + task.task3);
                Debug.Log("Task 4: " + task.task4);
                Debug.Log("UserID: " + task.iduser);*/
                newElement.GetComponent<setinfoitems>().addto(task);
            }

        }
        else
        {
            Debug.LogError(www.error);
        }
    }



}
[System.Serializable]
public class TaskInfo
{
    public string id;
    public string esm;
    public int task1;
    public int task2;
    public int task3;
    public int task4;
    public string iduser;
}
[System.Serializable]
public class TaskInfoList
{
    public List<TaskInfo> tasks;
}
