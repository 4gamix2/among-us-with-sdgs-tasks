using Unity.Netcode;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    private Transform target; // R�f�rence au transform du joueur � suivre
    public float smoothSpeed = 0.125f; // Vitesse de d�placement de la cam�ra
    public Vector3 offset; // D�calage de la cam�ra par rapport au joueur
    public Vector3 rotationOffset; // Offset de rotation suppl�mentaire de la cam�ra

    private NetworkObject targetPlayer;

    public Transform spawnPoint;



  




    void FixedUpdate()
    {
        if (targetPlayer != null)
        {
            // Calcule la position cible de la cam�ra en ajoutant le d�calage au joueur
            Vector3 desiredPosition = targetPlayer.transform.position + offset;

            // Interpole la position actuelle de la cam�ra vers la position cible de mani�re fluide
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

            // Applique l'offset de rotation suppl�mentaire � la rotation de la cam�ra
            Quaternion desiredRotation = Quaternion.Euler(rotationOffset);

            // Met � jour la position et la rotation de la cam�ra
            transform.position = smoothedPosition;
            transform.rotation = desiredRotation;

            // V�rifie si le joueur se d�place et active l'animation en cons�quence
            float moveSpeed = targetPlayer.GetComponent<Rigidbody>().velocity.magnitude;
            bool isMoving = moveSpeed > 0.1f; // Vous pouvez ajuster cette valeur selon votre besoin

            // Active ou d�sactive l'animation en fonction du mouvement du joueur
        }
    }

}
