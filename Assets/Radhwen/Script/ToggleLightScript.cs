using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleLightScript : MonoBehaviour
{
    public Light myLight;

    void Start()
    {
        myLight = GetComponent<Light>();
        myLight.enabled = false; // D�sactiver la lumi�re au d�but du jeu
    }

    void Update()
    {
        // Vous pouvez ajouter du code ici si n�cessaire
    }

    public void ToggleLight()
    {
        myLight.enabled = !myLight.enabled;
    }

    public void LightOff()
    {
        myLight.enabled = false;
    }
}
