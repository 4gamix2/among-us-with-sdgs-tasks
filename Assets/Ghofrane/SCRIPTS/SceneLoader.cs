using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class SceneLoader : MonoBehaviour
{
    public string sceneName; // Le nom de la sc�ne � charger
    public GameObject VoteCanvas;
   

    public void LoadSceneOnClick()
    {
        SceneManager.LoadScene(sceneName);
    }

    public void activerPanel()
    {
        VoteCanvas.SetActive(true);
    }
    public void desactiverPanel()
    {
        VoteCanvas.SetActive(false);
    }
}
