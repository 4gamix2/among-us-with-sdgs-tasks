using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class VoteManager : MonoBehaviour
{
    public TMP_Text candidateNameText;
    public Button voteButton;
    public Button cancelButton;
    private string selectedCandidate;
    public Button[] candidateButtons;
    public TextMeshProUGUI timerText;
    public float delayBeforeButtonsActive = 10f; // D�lai en secondes avant que les boutons ne deviennent actifs
    public float voteDuration = 20f; // Dur�e du vote en secondes
    private bool voteInProgress = false; // Indique si un vote est en cours
    public GameObject panel;
    public Button skipButton;


    private void Start()
    {
        // D�sactiver le bouton de vote et d'annulation au d�but
        voteButton.interactable = false;
        cancelButton.interactable = false;

        // Lancer la coroutine pour afficher le d�lai avant l'activation des boutons
        
    }

    // M�thode appel�e lorsqu'un candidat est s�lectionn�
    public void SelectCandidate(string candidateName)
    {
        if (!voteInProgress)
        {
            selectedCandidate = candidateName;
            candidateNameText.text = "The impostor is  " + selectedCandidate + "?";
            Debug.Log("The impostor is : " + selectedCandidate + "!!");
            voteButton.interactable = true; // Activer le bouton de vote une fois qu'un candidat est s�lectionn�
            cancelButton.interactable = true; // Activer le bouton d'annulation une fois qu'un candidat est s�lectionn�
            voteInProgress = true; // Indiquer qu'un vote est en cours
        }
    }

    // M�thode appel�e lorsqu'un vote est effectu�
    public void Vote()
    {
        if (!string.IsNullOrEmpty(selectedCandidate))
        {
            Debug.Log("vote for  " + selectedCandidate + " done successfully !");
            if (!candidateNameText.text.Contains("done"))
            {
                candidateNameText.text = "vote for " + selectedCandidate + " done successfully !";
                StartCoroutine(DisableVoteButtonAndPanel());

                // D�sactivez le bouton "Skip"
                if (skipButton != null)
                {
                    skipButton.interactable = false;
                }
            }
            // Ajoutez ici la logique pour enregistrer le vote pour le candidat s�lectionn�

            // D�sactiver tous les autres boutons de candidat
            foreach (Button button in candidateButtons)
            {
                button.interactable = false;
            }

            StartCoroutine(ResetVoteAfterDelay());
        }
        else if (!candidateNameText.text.Contains("done")) // V�rifie si le texte ne contient pas d�j� "done"
        {
            Debug.Log("None selected !");
            candidateNameText.text = "None selected!";

            // D�marrer une coroutine pour r�initialiser le texte apr�s quelques secondes
            StartCoroutine(ResetTextAfterDelay());
        }
    }


    // M�thode pour r�initialiser le texte apr�s quelques secondes
    private IEnumerator ResetTextAfterDelay()
    {
        yield return new WaitForSeconds(5f); // Attendre 5 secondes

        if (!string.IsNullOrEmpty(selectedCandidate)) // V�rifier si un candidat est s�lectionn�
        {
            candidateNameText.text = "vote for " + selectedCandidate + " done successfully !";
        }
        else
        {
            candidateNameText.text = "None selected!";
        }
    }

    // M�thode appel�e lors de l'annulation de la s�lection
    public void CancelSelection()
    {
        selectedCandidate = "";
        candidateNameText.text = "None selected!";
        voteButton.interactable = false; // D�sactiver le bouton de vote en cas d'annulation de la s�lection
        cancelButton.interactable = false; // D�sactiver le bouton d'annulation en cas d'annulation de la s�lection
        voteInProgress = false; // Indiquer qu'aucun vote n'est en cours
    }

    private IEnumerator CountdownAndActivateButtons()
    {
        float timer = delayBeforeButtonsActive;
        int secondsRemaining = Mathf.CeilToInt(voteDuration);

        // Mettre � jour le texte du timer tant que le d�lai initial n'est pas �coul�
        while (timer > 0 )
        {
            timer -= Time.deltaTime;

            // Si le d�lai initial est �coul� et qu'aucun vote n'est en cours, afficher "Proceeding in"
            if (timer <= 0 && !voteInProgress)
            {
                // Activer tous les boutons de candidat
                foreach (Button button in candidateButtons)
                {
                    button.interactable = true;
                }

                // Activer le panneau
                panel.SetActive(true);

                // Afficher le texte "Proceeding in" pendant le d�lai de vote initial
                while (secondsRemaining > 0)
                {
                    timerText.text = "Proceeding in: " + secondsRemaining;
                    yield return new WaitForSeconds(1f); // Attendre une seconde
                    secondsRemaining--;
                }

                // Sortir de la boucle lorsque le d�lai est �coul�
                break;
            }

            yield return null;
        }

        // Masquer le texte du timer une fois que les boutons sont activ�s
        timerText.gameObject.SetActive(false);
    }


    // M�thode pour r�initialiser le vote apr�s un d�lai
    private IEnumerator ResetVoteAfterDelay()
    {
        // Attendre un court d�lai avant de r�initialiser le vote
        yield return new WaitForSeconds(10f);

        // R�initialiser les donn�es du vote
        selectedCandidate = "";
        candidateNameText.text = "None selected!";
        voteButton.interactable = false;
        cancelButton.interactable = false;
    }

    // M�thode appel�e lors du clic sur un bouton de candidat
    public void OnCandidateButtonClick(TextMeshProUGUI buttonText)
    {
        string candidateName = buttonText.text;
        SelectCandidate(candidateName);
    }

    // M�thode pour d�sactiver le bouton de vote et le panneau apr�s un d�lai
    private IEnumerator DisableVoteButtonAndPanel()
    {
        yield return new WaitForSeconds(4f); // Attendre 5 secondes

        voteButton.interactable = false; // D�sactiver le bouton de vote
        cancelButton.interactable = false; // D�sactiver le bouton d'annulation
        // D�sactiver le panneau
        panel.SetActive(false);
    }
  


}