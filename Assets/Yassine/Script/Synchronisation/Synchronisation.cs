using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Synchronisation : NetworkBehaviour
{
    [SerializeField]
    private NetworkVariable<bool> quizCompleted = new NetworkVariable<bool>(false);

    [SerializeField]
    private NetworkVariable<bool> lumiereCompleted = new NetworkVariable<bool>(false);

    [SerializeField]
    private NetworkVariable<bool> dechetCompleted = new NetworkVariable<bool>(false);

    [SerializeField]
    private NetworkVariable<bool> disparaitreCompleted = new NetworkVariable<bool>(false);

    [SerializeField]
    private NetworkVariable<bool> cameraEnabled = new NetworkVariable<bool>(false);


    public bool QuizCompleted
    {
        get => quizCompleted.Value;
        set => quizCompleted.Value = value;
    }

    // M�thodes Getter et Setter pour lumieredesactive
    public bool LumiereCompleted
    {
        get => lumiereCompleted.Value;
        set => lumiereCompleted.Value = value;
    }

    // M�thodes Getter et Setter pour dechetCompleted
    public bool DechetCompleted
    {
        get => dechetCompleted.Value;
        set => dechetCompleted.Value = value;
    }

    // M�thodes Getter et Setter pour disparaitreCompleted
    public bool DisparaitreCompleted
    {
        get => disparaitreCompleted.Value;
        set => disparaitreCompleted.Value = value;
    }

    // M�thodes Getter et Setter pour cameraEnabled
    public bool CameraEnabled
    {
        get => cameraEnabled.Value;
        set => cameraEnabled.Value = value;
    }
}

