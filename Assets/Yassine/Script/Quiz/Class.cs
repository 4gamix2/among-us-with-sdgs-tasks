using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class QuestionData
{
    public List<Question> questions;
}

[Serializable]
public class Question
{
    public string question;
    public string[] reponses;
    public string reponse_correcte;
}
