using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotationScript : MonoBehaviour
{
    public float rotationSpeed = 5f;

    // M�thode pour faire pivoter l'objet
    public void RotateImage(float rotationAmount)
    {
        transform.Rotate(Vector3.forward, rotationAmount * rotationSpeed);
    }
}
