using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotationSlider : MonoBehaviour
{
    public RotationScript rotateImageScript;

    // Appel� lorsque l'utilisateur ajuste la valeur du slider
    public void OnRotationSliderChanged(Slider slider)
    {
        float rotationAmount = slider.value;
        rotateImageScript.RotateImage(rotationAmount);
    }
}
