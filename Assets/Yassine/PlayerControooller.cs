using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class PlayerControllerr : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private FixedJoystick _joystick;
    //[SerializeField] private Animator _animator;

    [SerializeField] private float _moveSpeed;

    private void FixedUpdate()
    {
        // Trouver le GameObject du Joystick dans la sc�ne
        GameObject _joystickGameObject = GameObject.Find("FixedJoystick");

        // V�rifier si le GameObject du Joystick a �t� trouv�
        if (_joystickGameObject != null)
        {
            // Obtenir le composant FixedJoystick du GameObject du Joystick
            _joystick = _joystickGameObject.GetComponent<FixedJoystick>();

            // V�rifier si le composant FixedJoystick a �t� trouv�
            if (_joystick != null)
            {
                // D�finir la v�locit� du Rigidbody en fonction de l'entr�e du Joystick
                _rigidbody.velocity = new Vector3(_joystick.Horizontal * _moveSpeed, _rigidbody.velocity.y, _joystick.Vertical * _moveSpeed);

                // V�rifier si le Joystick est en train d'�tre d�plac�
                if (_joystick.Horizontal != 0 || _joystick.Vertical != 0)
                {
                    // Faire en sorte que le personnage regarde dans la direction du mouvement
                    transform.rotation = Quaternion.LookRotation(_rigidbody.velocity);

                    // Activer l'animation de course
                    //_animator.SetBool("isRunning", true);
                }
                else
                {
                    // D�sactiver l'animation de course si le Joystick n'est pas d�plac�
                    //_animator.SetBool("isRunning", false);
                }
            }
            else
            {
                Debug.LogError("Le composant FixedJoystick n'a pas �t� trouv� sur le GameObject 'Fixed Joystick'. Assurez-vous que le GameObject a le bon nom.");
            }
        }
        else
        {
            Debug.LogError("Le GameObject 'Fixed Joystick' n'a pas �t� trouv� dans la sc�ne. Assurez-vous qu'il existe et qu'il est activ�.");
        }
    }
}