using System;
using UnityEngine;
using UnityEngine.UI;
using Button = UnityEngine.UIElements.Button;

public class PlayerMovement : MonoBehaviour
{
    //public float speed = 5f;
    public GameObject button;
    public GameObject button1;
    public GameObject button2;
    public GameObject panel;
    bool activated = false;
    //public float cameraDistance = 10f;
    //public float cameraHeightOffset = 100f;
    private bool isColliding1 = false;
    private bool isColliding = false;
    private bool isColliding2 = false;
    public GameObject activationButton; // Référence au bouton d'activation dans l'interface utilisateur
    public GameObject deactivationButton; // Référence au bouton de désactivation dans l'interface utilisateur
    bool cylinderCollision = false; // Indique si une collision avec le cylindre est en cours
    bool capsuleCollision = false; // Indique si une collision avec la capsule est en cours
    //private Transform mainCameraTransform;


    private void Start()
    {
        activationButton.SetActive(false);
        deactivationButton.SetActive(false);

        button1.SetActive(false);
        button2.SetActive(false);
        button.SetActive(false);
        panel.SetActive(false);

        //mainCameraTransform = Camera.main.transform;
    }

    //private void Update()
    //{
    //    if (!isColliding || (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0))
    //    {
    //        float horizontalInput = Input.GetAxis("Horizontal");
    //        float verticalInput = Input.GetAxis("Vertical");

    //        Vector3 movement = new Vector3(horizontalInput, 0f, verticalInput) * speed * Time.deltaTime;
    //        transform.Translate(movement);

    //        Vector3 desiredCameraPosition = transform.position - transform.forward * cameraDistance + Vector3.up * cameraHeightOffset;
    //        mainCameraTransform.position = Vector3.Lerp(mainCameraTransform.position, desiredCameraPosition, Time.deltaTime * speed);
    //    }
    //}

    //private void OnCollisionEnter(Collision collision)
    //{
    //    // Si le joueur entre en collision avec le cube, activer le bouton
    //    if (collision.gameObject.CompareTag("Cube"))
    //    {
    //        button.SetActive(true);
    //        isColliding = true; // Indique que le joueur est en collision avec le cube

    //    }
    //    if (collision.gameObject.CompareTag("Sphere"))
    //    {
    //        button1.SetActive(true);
    //        isColliding1 = true; // Indique que le joueur est en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Memory"))
    //    {
    //        button2.SetActive(true);
    //        isColliding2 = true; // Indique que le joueur est en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Cylinder"))
    //    {
    //        activationButton.SetActive(true);
    //        cylinderCollision = true; // Indique que le joueur est en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Capsule"))
    //    {
    //        deactivationButton.SetActive(true);
    //        capsuleCollision = true; // Indique que le joueur est en collision avec le cube
    //    }
    //}

    //private void OnCollisionExit(Collision collision)
    //{
    //    // Si le joueur quitte la collision avec le cube, désactiver le bouton
    //    if (collision.gameObject.CompareTag("Cube"))
    //    {
    //        button.SetActive(false);
    //        isColliding = false; // Indique que le joueur n'est plus en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Sphere"))
    //    {
    //        button1.SetActive(false);
    //        isColliding1 = false; // Indique que le joueur n'est plus en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Memory"))
    //    {
    //        button2.SetActive(false);
    //        isColliding2 = false; // Indique que le joueur n'est plus en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Cylinder"))
    //    {
    //        activationButton.SetActive(false);
    //        cylinderCollision = false; // Indique que le joueur n'est plus en collision avec le cube
    //    }
    //    if (collision.gameObject.CompareTag("Capsule"))
    //    {
    //        deactivationButton.SetActive(false);
    //        capsuleCollision = false; // Indique que le joueur n'est plus en collision avec le cube

    //    }
    //}


    public void ActivatePanel()
    {
        panel.SetActive(true);
        button.SetActive(false);
    }


}
