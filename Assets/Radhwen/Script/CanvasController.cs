using UnityEngine;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{

    public delegate void CanvasOpenedEventHandler();
    public static event CanvasOpenedEventHandler OnCanvasOpened;

    public delegate void CanvasClosedEventHandler();
    public static event CanvasClosedEventHandler OnCanvasClosed;

    // M�thode pour ouvrir le Canvas
    public void OpenCanvas()
    {
        // Activer le Canvas
        gameObject.SetActive(true);

        // D�clencher l'�v�nement de Canvas ouvert
        OnCanvasOpened?.Invoke();
    }

    // M�thode pour fermer le Canvas
    public void CloseCanvas()
    {

        // D�sactiver le Canvas
        gameObject.SetActive(false);

        // D�clencher l'�v�nement de Canvas ferm�
        OnCanvasClosed?.Invoke();

    }
}
