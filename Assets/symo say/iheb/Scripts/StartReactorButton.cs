using UnityEngine;
using UnityEngine.UI;

public class StartReactorButton : MonoBehaviour
{
    private int index;
    private Button button;
    private StartReactorTask parentTask;





    public void Initialize(int index, StartReactorTask parentTask)
    {
        this.index = index;

        this.parentTask = parentTask;

        if (button == null)
        {

            button = GetComponent<Button>();
            button.onClick.AddListener(OnButtonPressed);
        }


    }

    public void Toggle(bool isActive)
    {
        button.interactable = isActive;
    }
    private void OnButtonPressed()
    {
        if (parentTask != null)
        {
            parentTask.OnButtonPressed(index);
        }
        else
        {
            Debug.LogError("parentTask is null in StartReactorButton");
        }
    }

}