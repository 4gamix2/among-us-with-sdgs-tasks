using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class PlayerHealth : NetworkBehaviour
{
    [SerializeField] private int maxHealth = 100;
    public NetworkVariable<int> currentHealth = new NetworkVariable<int>(0);

    public Text healthText;

    PlayerManager playerManager;

    private void Start()
    {
        if (!IsLocalPlayer)
        { return; }
        PlayerPrefs.DeleteKey("iduser");
        // Subscribe to changes in the currentHealth variable
        currentHealth.OnValueChanged += OnHealthChanged;

        // Initialize currentHealth on the server
        if (IsServer)
        {
            currentHealth.Value = maxHealth;
        }
        playerManager = FindObjectOfType<PlayerManager>();

    }

    void Update()
    {

        Debug.Log((PlayerPrefs.GetInt("idd") == (int)this.NetworkBehaviourId));
        Debug.Log((int)this.NetworkBehaviourId);
        Debug.Log(PlayerPrefs.GetInt("idd"));

        if (PlayerPrefs.GetInt("idd") == (int)NetworkBehaviourId)
        {
            PlayerPrefs.DeleteKey("idd");
            Destroy(gameObject);

        }

        if (PlayerPrefs.HasKey("iduser"))
        {

            // currentHealth.Value-=1;
            //  Debug.Log( PlayerPrefs.GetInt("iduser"));
            SendButtonClickToServerRpc(PlayerPrefs.GetInt("iduser"));
            PlayerPrefs.DeleteKey("iduser");

        }
        if (Input.GetButton("Jump"))
        {
            SendButtonClickToServerRpc(PlayerPrefs.GetInt("iduser"));

        }
    }

    // Apply damage to the player
    public void TakeDamage(int damage)
    {
        if (IsServer)
        {
            currentHealth.Value -= damage;
        }
        currentHealth.Value -= damage;
    }

    // Update UI when health changes
    private void OnHealthChanged(int oldValue, int newValue)
    {
        if (healthText != null)
        {
            healthText.text = "Health: " + newValue.ToString();
        }
    }

    // Clean up event subscription when the object is destroyed
    private void OnDestroy()
    {
        currentHealth.OnValueChanged -= OnHealthChanged;
    }

    // Server command to damage the player
    [ServerRpc]
    void DamagePlayerServerRpc(int damage)
    {
        print("i get you ");
        // Only apply damage on the server
        currentHealth.Value -= damage;
    }

    // Public method to be called when player is attacked
    public void AttackPlayer(int damage)
    {
        // Make sure this is called from the server
        if (!IsServer)
            return;

        // Damage the target player
        DamagePlayerServerRpc(damage);
    }


    [ServerRpc(RequireOwnership = false)]
    void SendButtonClickToServerRpc(int astra)

    {
        PlayerHealth[] playerHealthComponents = FindObjectsOfType<PlayerHealth>();

        // Add each player to the dictionary
        foreach (PlayerHealth playerHealth in playerHealthComponents)
        {
            if ((int)playerHealth.NetworkObjectId == astra)
                playerHealth.currentHealth.Value -= 1;
            //  Debug.Log(playerHealth.currentHealth.Value);
            //  players.Add((uint)playerHealth.NetworkObjectId, playerHealth);
            Debug.Log(playerHealth.NetworkObjectId + " - " + playerHealth.currentHealth.Value);
        }
        // This RPC will be executed on the server
        Debug.Log("RPC executed on server: Hello from client");
    }
}
