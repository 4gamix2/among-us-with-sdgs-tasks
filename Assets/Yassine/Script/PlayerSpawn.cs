using UnityEngine;

public class PlayerSpawnManager : MonoBehaviour
{
    public Transform[] spawnPoints; // Liste des points de spawn des joueurs

    private void Awake()
    {
        // Assurez-vous qu'il y a au moins un point de spawn d�fini
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("Aucun point de spawn n'est d�fini dans le gestionnaire de spawn du joueur.");
        }
    }

    // Fonction pour r�cup�rer un point de spawn al�atoire
    public Transform GetRandomSpawnPoint()
    {
        // V�rifiez s'il y a des points de spawn disponibles
        if (spawnPoints.Length == 0)
        {
            Debug.LogWarning("Aucun point de spawn n'est d�fini.");
            return null;
        }

        // S�lectionnez un point de spawn al�atoire dans la liste
        int randomIndex = Random.Range(0, spawnPoints.Length);
        return spawnPoints[randomIndex];
    }

    // Fonction pour r�cup�rer un point de spawn sp�cifique par son index
    public Transform GetSpawnPointAtIndex(int index)
    {
        // V�rifiez si l'index est valide
        if (index < 0 || index >= spawnPoints.Length)
        {
            Debug.LogWarning("Index de point de spawn invalide.");
            return null;
        }

        // Retournez le point de spawn correspondant � l'index donn�
        return spawnPoints[index];
    }
}
