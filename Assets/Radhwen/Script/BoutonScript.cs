using UnityEngine;
using UnityEngine.UI;

public class BoutonScript : MonoBehaviour
{
    // R�f�rence vers le Canvas � d�truire
    public GameObject canvasADetruire;
    public GameObject objetAAfficher;
    public GameObject canvasAAfficherOuMasquer;

    private void Start()
    {
        // Assurez-vous que le Canvas est assign� dans l'�diteur Unity
        if (canvasADetruire == null)
        {
            Debug.LogError("Canvas � d�truire non assign�.");
            return;
        }

        // Ajoutez une fonction � appeler lors du clic sur le bouton
        GetComponent<Button>().onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        // D�truisez le Canvas lorsque vous cliquez sur le bouton
        Destroy(canvasADetruire);
    }

    // Fonction appel�e lors du clic sur le bouton
    public void OnBoutonClique()
    {
        if (objetAAfficher != null)
        {
            objetAAfficher.SetActive(true);
        }

        if (canvasAAfficherOuMasquer != null)
        {
            // Inversez l'�tat actuel du canvas (actif ou inactif)
            bool nouvelEtat = !canvasAAfficherOuMasquer.activeSelf;
            canvasAAfficherOuMasquer.SetActive(nouvelEtat);
        }
    }
}
