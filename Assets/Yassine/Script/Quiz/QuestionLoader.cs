using UnityEngine;
using System.Collections.Generic;

public class QuestionLoader : MonoBehaviour
{
    public GameObject interfaceObject;
    public TextAsset questionData; // Fichier JSON contenant les questions

    void Start()
    {
        // Charger les questions depuis le fichier JSON lorsque le jeu d�marre
        List<Question> questions = LoadQuestions();

        // Utiliser les questions charg�es ici
        foreach (Question question in questions)
        {
            Debug.Log("Question: " + question.question);
            Debug.Log("R�ponses:");
            foreach (string reponse in question.reponses)
            {
                Debug.Log(" - " + reponse);
            }
            Debug.Log("R�ponse correcte: " + question.reponse_correcte);
        }
        interfaceObject.SetActive(false);
    }

    List<Question> LoadQuestions()
    {
        // Charger le contenu du fichier JSON en tant que cha�ne de caract�res
        string json = LoadJSONFromFile(questionData);

        // D�s�rialiser le contenu JSON en une instance de QuestionData
        QuestionData questionDataInstance = JsonUtility.FromJson<QuestionData>(json);

        // Acc�der � la liste de questions � partir de l'instance de QuestionData
        List<Question> questions = questionDataInstance.questions;

        return questions;
    }

    string LoadJSONFromFile(TextAsset jsonFile)
    {
        // V�rifier si le fichier existe
        if (jsonFile != null)
        {
            // Charger le contenu du fichier JSON en tant que cha�ne de caract�res
            string json = jsonFile.text;
            return json;
        }
        else
        {
            Debug.LogError("Le fichier JSON est manquant !");
            return null;
        }
    }



    public void OpenInterface()
    {
        if (interfaceObject != null)
        {
            interfaceObject.SetActive(true); // Activer l'objet contenant l'interface
        }
        else
        {
            Debug.LogError("L'objet d'interface n'est pas assign� !");
        }
    }

    // Fonction pour fermer l'interface
    public void CloseInterface()
    {
        if (interfaceObject != null)
        {
            interfaceObject.SetActive(false); // D�sactiver l'objet contenant l'interface
        }
        else
        {
            Debug.LogError("L'objet d'interface n'est pas assign� !");
        }
    }
}
