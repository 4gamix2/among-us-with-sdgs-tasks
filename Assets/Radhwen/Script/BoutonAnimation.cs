using UnityEngine;
using UnityEngine.UI;

public class BoutonAnimation : MonoBehaviour
{
    public Animator animator;

    void Start()
    {
        // Assurez-vous d'attacher l'Animator au GameObject ou attribuez-le via l'inspecteur.
        if (animator == null)
            animator = GetComponent<Animator>();
    }

    public void OnButtonClick()
    {
        // Définissez le déclencheur pour déclencher l'animation.
        animator.SetTrigger("ClicSurBouton");
    }
}
