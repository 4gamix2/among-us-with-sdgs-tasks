using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ApiManager : MonoBehaviour
{
    // URL API REST
    private string apiUrl = "http://localhost:3000/api/skins";
    

    public SkinUIManager skinUIManager; // R�f�rence au SkinUIManager

    // send request HTTP to GET all skins
    public IEnumerator GetSkins()
    {
        // Create a request to the API
        WWW request = new WWW(apiUrl);

        // Wait for the request to finish
        yield return request;

        // Check if there is an error
        if (request.error != null)
        {
            Debug.LogError("Error: " + request.error);
        }
        else
        {
            // Parse the JSON response
            List<Skin> skins = JsonUtility.FromJson<List<Skin>>(request.text);
            
            // Update the UI
            skinUIManager.UpdateSkins(skins);

            
        }
    }
    

}

public class SkinUIManager : MonoBehaviour
{
    public void UpdateSkins(List<Skin> skins)
    {
        // Update the UI
        foreach (Skin skin in skins)
        {
            Debug.Log(skin.name);
        }

    }
}

public class Skin
{
    public string _id;
    public string name;
    public string description;
    public string image;
    public int price;
    public bool isPurchased;
    public bool isEquipped;
    public string createdAt;
    public string updatedAt;
    public string __v;
}