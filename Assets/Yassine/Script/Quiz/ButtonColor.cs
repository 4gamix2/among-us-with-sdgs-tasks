using TMPro;

using UnityEngine;
using UnityEngine.UI;

public class TextColorChanger : MonoBehaviour
{

    public Button buttonVert;
    public Button buttonRouge;
    public TextMeshProUGUI text;
    private bool OnButtonClick = false;
    



    public void colorToGreen()
    {
        text.color = Color.green;
        Invoke("colorToBlack", 1f);
    }
    public void colorToRed()
    {
        text.color = Color.red;
        Invoke("colorToBlack", 1f);
    }

    private void colorToBlack()
    {
        text.color = Color.black;
    }

    //void Start()
    //{

    //    buttonVert.onClick.AddListener(colorToGreen);
    //    buttonRouge.onClick.AddListener(colorToRed);
    //}

    //void Update()
    //{
    //    if (OnButtonClick)
    //    {
    //        text.color = Color.green;
    //    }
    //    else
    //    {
    //        text.color = Color.red;
    //    }
    //}
}
