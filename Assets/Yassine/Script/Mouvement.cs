using UnityEngine;
using UnityEngine.UI;

public class DeplacementJoueurr : MonoBehaviour
{
    public float vitesseDeplacement = 5f; // Vitesse de d�placement du joueur
    public float vitesseLerp = 5f;
    public Animator animator;
    
    void Start()
    {
        // Assurez-vous d'avoir un Animator attach� � l'objet
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("Veuillez attacher un Animator � l'objet.");
        }
    }
    void Update()
    {

        var oldPosition = transform.position;
        // V�rifie si le bouton droit de la souris est enfonc�
        if (Input.GetMouseButton(1))
        {
            // Obtient la position de la souris dans l'espace du monde
            Vector3 positionSouris = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.y));

            // Fixe la position sur le plan du sol (Y = 0)
            Vector3 positionCible = new Vector3(positionSouris.x, 0f, positionSouris.z);

            // Utilise l'interpolation lin�aire pour un mouvement fluide
            transform.position = Vector3.Lerp(transform.position, positionCible, vitesseLerp * Time.deltaTime);

            // Calcule la direction entre la position actuelle de Sboui et la position de la souris
            Vector3 direction = positionCible - transform.position;

            // Calcule l'angle d'inclinaison en radians et le convertit en degr�s
            float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

            // Cr�e une rotation � partir de l'angle calcul�
            Quaternion rotation = Quaternion.Euler(0f, angle, 0f);

            // Applique la rotation � Sboui
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, vitesseLerp * Time.deltaTime);


        }
        if (transform.position != oldPosition)
        {
            animator.SetBool("Move", true);
        }
        else
        {
            animator.SetBool("Move", false);
        }
    }
}
