using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneCode : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject phoneui;

    private void OnTriggerEnter(Collider other) {
        phoneui.SetActive(true);
        
    }

    private void OnTriggerExit(Collider other) {
        phoneui.SetActive(false);
    }


}
