using UnityEngine;
using UnityEngine.UI;

public class GestionBoutons : MonoBehaviour
{
    public Button bouton1;  // Premier bouton visible initialement
    public Button bouton2;  // Deuxi�me bouton initialement d�sactiv�

    private void Start()
    {
        // Active le premier bouton au d�marrage
        if (bouton1 != null)
            bouton1.gameObject.SetActive(true);

        // D�marre le processus de changement de boutons apr�s 5 secondes
        Invoke("ActiverDeuxiemeBouton", 5f);
    }

    private void ActiverDeuxiemeBouton()
    {
        // D�sactive le premier bouton
        if (bouton1 != null)
            bouton1.gameObject.SetActive(false);

        // Active le deuxi�me bouton
        if (bouton2 != null)
            bouton2.gameObject.SetActive(true);
    }
}
