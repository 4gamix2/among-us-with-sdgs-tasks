using UnityEngine;

public class GestionSpotlightt : MonoBehaviour
{
    public Light projecteur;
    public Light lumierePrincipale;

    void Start()
    {
        // Assurez-vous que le projecteur et la lumi�re principale sont assign�s dans l'Inspector Unity
        if (projecteur == null || lumierePrincipale == null)
        {
            Debug.LogError("Veuillez assigner le projecteur de lumi�re et la lumi�re principale dans l'Inspector Unity.");
        }
    }

    void Update()
    {
        // V�rifiez si la luminosit� de la lumi�re principale est �gale � z�ro
        if (lumierePrincipale.enabled == false)
        {
            // Activer le projecteur de lumi�re
            projecteur.enabled = true;
            
        }
        else
        {
            // D�sactiver le projecteur de lumi�re
            projecteur.enabled = false;
        }
    }
}
