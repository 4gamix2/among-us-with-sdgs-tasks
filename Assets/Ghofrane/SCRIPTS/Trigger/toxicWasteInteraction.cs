﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class toxicWasteInteraction : MonoBehaviour
{
    public Button toxicWasteButton;
    // Start is called before the first frame update
    public MainMenu mainMenu;
    public void Start()
    {
        mainMenu = FindObjectOfType<MainMenu>();
        toxicWasteButton.gameObject.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        // V�rifie si le joueur entre en collision avec la sph�re
        if (other.CompareTag("Player") && mainMenu.PlayerRole == "Imposteur")
        {
            toxicWasteButton.gameObject.SetActive(true); // Active le bouton
        }
    }

    void OnTriggerExit(Collider other)
    {
        // V�rifie si le joueur quitte la collision avec la sph�re
        if (other.CompareTag("Player"))
        {
            toxicWasteButton.gameObject.SetActive(false); // D�sactive le bouton
        }
    }
}
