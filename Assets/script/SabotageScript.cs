using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class SabotageScript : NetworkBehaviour
{
    // Start is called before the first frame update


    public GameObject menuview;
    public GameObject cam1;
    public GameObject ligthingS;
    public GameObject door;

    public NetworkVariable<bool> isActive = new NetworkVariable<bool>(true);




    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(   isActive.Value==false){
        ligthingS.SetActive(false);
       }
       else{
         ligthingS.SetActive(true);
       }
    }


  public void menuui(){
            
            if(menuview.activeSelf)
            {
               menuview.SetActive(false);
            }else{
                menuview.SetActive(true); 
            }

    }

    public void cameraSabotage(){
     
            
            if(cam1.activeSelf)
            {
               cam1.SetActive(false);
                StartCoroutine(ActivateCam1AfterDelay(30f,cam1));
            }

    }

       public void tournofLight(){
            
            if(ligthingS.activeSelf)
            {
               isActive.Value=false;
              //  StartCoroutine(lightactive(30f,ligthingS));
            }

    }


       public void doorlockandunlock()
       {
           door.GetComponent<Animator>().enabled = false;
        door.GetComponent<Animator>().enabled = true;



    }

    IEnumerator ActivateCam1AfterDelay(float delay ,GameObject nameSabotage )
    {
        yield return new WaitForSeconds(delay);
        nameSabotage.SetActive(true);
    }

       IEnumerator lightactive(float delay ,GameObject nameSabotage )
    {
        yield return new WaitForSeconds(delay);
         isActive.Value=true;
    }


}
