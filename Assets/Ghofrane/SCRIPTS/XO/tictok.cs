using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TicToc : MonoBehaviour
{
    public string[] matrix = new string[9];
    public List<int> choice = new List<int>();
    public TextMeshProUGUI resultText;

    private void Start()
    {
        int turn = Random.Range(0, 2); 
        if (turn == 0)
        {
            ComputerPlay();
        }
    }

    public void ComputerPlay()
    {
        MajChoice();

        int nr = choice[Random.Range(0, choice.Count)];
       
        GameObject.Find(nr.ToString()).transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "O";
        matrix[nr] = "O";
        GameObject.Find(nr.ToString()).GetComponent<Button>().interactable = false;
   if(verification("O"))
        {
            resultText.text = "you lose";
            StartCoroutine(DeactivateCanvasAfterDelay(3f)); // D�sactiver le canvas apr�s 3 secondes
            Debug.Log("O a gagn�");
        }
   else
        {
            MajChoice();
        }
    }
    IEnumerator DeactivateCanvasAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject.Find("Canvas1").SetActive(false); // D�sactiver le canvas
    }

    void MajChoice()
    {
        choice.Clear();
        for(int i = 0; i < matrix.Length; i++)
        {
            if (matrix[i]=="")
            {
                choice.Add(i);
            }
        }
        if(choice.Count == 0) 
        {
             resultText.text = "Match Null"; // Afficher le r�sultat du match nul
            Debug.Log("Match Null");
            StartCoroutine(DeactivateCanvasAfterDelay(3f)); // D�sactiver le canvas apr�s 3 secondes
            return;
        }
    }
    public bool verification(string player)
    {
        if (matrix[0]==player && matrix[1]==player && matrix[2]==player ||
            matrix[3] == player && matrix[4] == player && matrix[5] == player ||
            matrix[6] == player && matrix[7] == player && matrix[8] == player ||
           matrix[0] == player && matrix[4] == player && matrix[8] == player ||
           matrix[2] == player && matrix[4] == player && matrix[6] == player ||
           matrix[0] == player && matrix[3] == player && matrix[6] == player ||
           matrix[1] == player && matrix[4] == player && matrix[7] == player ||
           matrix[2] == player && matrix[5] == player && matrix[8] == player )

        {
            return true;
        }
        else 
        {
            return false;
        }
    }
  
}
