using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Networking;

public class ExpUIManager : MonoBehaviour
{
    public ApiManager apiManager;
    public Text ExpGoldCollectedText;

    public List<ExpData> exps = new List<ExpData>();

    private void Start()
    {
        //StartCoroutine(apiManager.GetExps());
    }

    public void UpdateExpUI(int index)
    {
        ExpGoldCollectedText.text = exps[index].GoldCollected.ToString();
    }

 //   private IEnumerator LoadImage(string url)
 //   {
 //       using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
 //    {
 //        yield return www.SendWebRequest();
 //        if (www.result == UnityWebRequest.Result.Success)
 //        {
 //            Texture2D texture = DownloadHandlerTexture.GetContent(www);
 //            ExpImage.texture = texture;
 //        }
 //        else
 //        {
 //            Debug.LogError("Failed to download image: " + www.error);
 //        }
 //    }
 //}

    public void SetExps(List<ExpData> ExpDataList)
    {
        exps = ExpDataList;
        UpdateExpUI(0); // Met � jour l'interface avec le premier exp par d�faut
    }
}
[System.Serializable]
public class ExpData
{
    public int GoldCollected;

    // Constructeur param�tr�
    public ExpData(int GoldCollected)
    {
        this.GoldCollected = GoldCollected;
    }
}

