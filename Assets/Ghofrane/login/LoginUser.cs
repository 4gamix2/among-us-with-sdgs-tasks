using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[System.Serializable]
public class ResetTokenResponse
{

    public string resetToken;
}
public class loginResponse
{
    public string _id;
    public string name,email,password;
    public int __v;
}

public class LoginUser : MonoBehaviour
{
    [SerializeField] TMP_InputField emailText;
    [SerializeField] TMP_InputField passwordText;
    [SerializeField] TMP_Text welcomeText;
    [SerializeField] TMP_InputField codeInputField;
    [SerializeField] TMP_Text validationText;
    [SerializeField] TMP_InputField newPasswordText;
    [SerializeField] TMP_InputField usernameText;
    [SerializeField] TMP_Text successMessageText;

    [SerializeField] GameObject tokenImage; // R�f�rence � l'image que vous voulez activer
    [SerializeField] GameObject resetImage; // R�f�rence � l'image que vous voulez activer apr�s 2 secondes
    public string responseText;
    public string resetToken;

    public Button submit123;
    string loginURL = "http://localhost:3000/user/login";
    string forgotPasswordURL = "http://localhost:3000/user/forgot-password";

    string updatePasswordURL = "http://localhost:3000/user/updatePassword"; // URL de l'API de mise � jour du mot de passe
    private string generatedCode; // Variable pour stocker le code g�n�r�
    private int min = 0;
    private int max = 9;
    private bool waitingToLoadNextScene = false;
    private void Awake()
    {
        submit123.onClick.AddListener(UpdatePassword);
    }
    private void Update()
    {
        Debug.Log(resetToken);
    }
    public void UpdatePassword()
    {
        string username = usernameText.text.ToString();
        string newPassword = newPasswordText.text.ToString();

        //if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(newPassword))
        //{
        //    Debug.Log("Fields must not be empty");
        //    return;
        //}

        StartCoroutine(UpdatePasswordRequest(username, newPassword));
    }

    IEnumerator UpdatePasswordRequest(string username, string newPassword)
    {
        string updatePasswordURLWithParams = updatePasswordURL + "/" + username + "/" + newPassword;

        using (UnityWebRequest request = UnityWebRequest.Put(updatePasswordURLWithParams, ""))
        {
            request.SetRequestHeader("Content-Type", "application/json");

            yield return request.SendWebRequest();

            if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.LogError("Network error: " + request.error);
                SetWelcomeText("Failed to update password");
                successMessageText.text = "Failed to update password";
            }
            else
            {
                if (request.responseCode == 200)
                {
                    Debug.Log("Mot de passe mis � jour avec succ�s!");
                    SetWelcomeText("Password updated successfully!");
                    successMessageText.text = "Password updated successfully!";
                }
                else
                {
                    Debug.LogError("Failed to update password: " + request.downloadHandler.text);
                    SetWelcomeText("Failed to update password");
                    successMessageText.text = "Failed to update password";
                }
            }
        }
    }
    public void ReturnToLoginImageAndEnterLogin()
    {
        if (resetImage != null)
            resetImage.SetActive(false); // D�sactivez l'image de r�initialisation

        if (tokenImage != null)
            tokenImage.SetActive(false); // D�sactivez l'image du jeton ou du code

        // Appelez la fonction Enter
        Enter();


    }

    public void Enter()
    {
        string email = emailText.text;
        string password = passwordText.text;

        if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
        {
            Debug.Log("Les champs ne doivent pas �tre vides");
            return;
        }

        StartCoroutine(Login(email, password));
    }

    public void ForgotPassword()
    {
        string email = emailText.text;

        if (string.IsNullOrEmpty(email))
        {
            Debug.Log("Le champ d'email ne doit pas �tre vide");
            return;
        }

        StartCoroutine(ForgotPasswordRequest(email));
    }

    IEnumerator Login(string email, string password)
    {
        WWWForm form = new WWWForm();
        loginResponse userdata = new loginResponse();
        string jsonData = JsonUtility.ToJson(userdata);
        form.AddField("email", email);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post(loginURL, form))
        {

            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log("Network error: " + www.error);
                SetWelcomeText("Login failed");
            }
            else
            {
                if (www.responseCode == 200)
                {
                    
                    Debug.Log("Successful connection !");
                    Debug.Log("www.downloadHandler.text"+ www.downloadHandler.text);
                    loginResponse response = JsonUtility.FromJson<loginResponse>(www.downloadHandler.text);
                    PlayerPrefs.SetString("name", response.name);
                    Debug.Log("www.downloadHandler.text"+ www.downloadHandler.text);
                    SetWelcomeText("Welcome to the game");
                    {
                        waitingToLoadNextScene = true;
                        StartCoroutine(LoadNextSceneAfterDelay());
                    }
                }
                else
                {
                    Debug.Log("Login failed: " + www.downloadHandler.text);
                    SetWelcomeText("Login failed");
                }
            }
        }
    }
    IEnumerator LoadNextSceneAfterDelay()
    {
        yield return new WaitForSeconds(2f);

        if (waitingToLoadNextScene)
        {
            // Replace "SceneName" with the actual name of your next scene
            SceneManager.LoadScene("MainMenu");
        }
    }
    private System.Random random = new System.Random();
    public string GenerateRandomCode()
    {
        string code = "";
        for (int i = 0; i < 4; i++)
        {
            int randomNumber = UnityEngine.Random.Range(min, max + 1);
            code += randomNumber.ToString();
        }
        return code;
    }
    IEnumerator ActivateResetImageAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (resetImage != null)
            resetImage.SetActive(true);
    }
    IEnumerator ActivatetokenImageAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (tokenImage != null)
            tokenImage.SetActive(true);
    }
    public void Submit()
    {
        // Get the user-entered code from the input field
        string enteredCode = codeInputField.text.ToString();



        // Check if the user entered a code
        if (string.IsNullOrEmpty(enteredCode))
        {
            validationText.text = "Please enter a passcode.";
            return;
        }

        // Check if the entered code is the correct length (4 digits)
        if (enteredCode.Length != 4)
        {
            validationText.text = "The code must be 4 digits long.";
            return;
        }

        // Replace "your_generated_code" with the actual way you store the generated code
        string generatedCode = "5348";

        // Validate the entered code
        if (enteredCode == resetToken)
        {

            validationText.text = "Correct code!";
            StartCoroutine(ActivateResetImageAfterDelay(2f));
        }
        else
        {
            validationText.text = "Invalid code. Try Again.";
        }
    }

    IEnumerator ForgotPasswordRequest(string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);

        using (UnityWebRequest www = UnityWebRequest.Post(forgotPasswordURL, form))
        {


            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                Debug.Log("Network error: " + www.error);
                SetWelcomeText("�chec de l'envoi de l'e-mail de r�initialisation du mot de passe");
            }
            else
            {
                if (www.responseCode == 200)
                {
                    Debug.Log("E-mail de r�initialisation du mot de passe envoy� avec succ�s!");

                    string responseText = www.downloadHandler.text;

                    // Parse the response text
                    ResetTokenResponse responseObject = JsonUtility.FromJson<ResetTokenResponse>(responseText);

                    // Extract the reset token
                    resetToken = responseObject.resetToken;

                    SetWelcomeText("Password reset email sent successfully!");
                    StartCoroutine(ActivatetokenImageAfterDelay(2f));
                }
                else
                {
                    Debug.Log("�chec de l'envoi de l'e-mail de r�initialisation du mot de passe: " + www.downloadHandler.text);
                    SetWelcomeText("Failed to send password reset email");
                }
            }
        }

    }


    void SetWelcomeText(string message)
    {
        welcomeText.text = message;
    }
}