using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCountUI : MonoBehaviour
{
    public Text playerCountText; // Texte pour afficher le nombre de joueurs
    private int playerCount = 6; // Nombre initial de joueurs

    void Start()
    {
        UpdatePlayerCountText();
    }

    void UpdatePlayerCountText()
    {
        playerCountText.text = "Nombre de joueurs : " + playerCount;
    }

    public void IncreasePlayerCount()
    {
        playerCount++;
        UpdatePlayerCountText();
    }

    public void DecreasePlayerCount()
    {
        if (playerCount > 1) // Assurez-vous qu'il y a au moins un joueur
        {
            playerCount--;
            UpdatePlayerCountText();
        }
    }

    public int GetPlayerCount()
    {
        int count = int.Parse(playerCountText.text.Split(':')[1].Trim());
        return count;
    }

}
