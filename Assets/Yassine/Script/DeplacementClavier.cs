using Unity.Netcode;
using UnityEngine;


[RequireComponent(typeof(NetworkObject))]
public class DeplacementJoueur :  NetworkBehaviour
{
    public float vitesse = 30.0f;
    private AudioSource eatCoin;
    private Transform joueurTransform;
    private CharacterController characterController;
    [SerializeField]
    private NetworkVariable<Vector3> networkPositionDirection = new NetworkVariable<Vector3>();
    [SerializeField]
    private NetworkVariable<PlayerState> networkPlayerState = new NetworkVariable<PlayerState>();

    private Vector3 oldInputPosition = Vector3.zero;
    private Vector3 oldInputRotation = Vector3.zero;
    private PlayerState oldPlayerState = PlayerState.Idle;

    private void Awake()
    {
        characterController = GetComponent<CharacterController>();
        
    }
    private void ClientMoveAndRotate()
    {
        if (networkPositionDirection.Value != Vector3.zero)
        {
            characterController.SimpleMove(networkPositionDirection.Value);
        }
        
    }
    private void ClientVisuals()
    {
        if (oldPlayerState != networkPlayerState.Value)
        {
            oldPlayerState = networkPlayerState.Value;
        }
    }
    void Start()
    {
        if (IsServer && IsOwner)
        {
            // R�cup�rer le transform du joueur en utilisant GetComponent<Transform>()
            joueurTransform = GetComponent<Transform>();

            // Initialiser l'�chelle � (50, 50, 50)
            joueurTransform.localScale = new Vector3(50f, 50f, 50f);
        }
    }
    void Update()
    {
        if (IsServer && IsOwner) 
        { 
        
            float deplacementHorizontal = Input.GetAxis("Horizontal");
            float deplacementVertical = Input.GetAxis("Vertical");

            Vector3 deplacement = new Vector3(deplacementHorizontal, 0.0f, deplacementVertical) * vitesse * Time.deltaTime;
            Vector3 rotationInitiale = new Vector3(40.315f, -12.788f, -292.089f);

            RaycastHit hit;
            if (!Physics.Raycast(transform.position, deplacement, out hit, deplacement.magnitude))
            {
                transform.position += deplacement;
            }

            if (Mathf.Abs(deplacementHorizontal) > 0.1f || Mathf.Abs(deplacementVertical) > 0.1f)
            {
                Vector3 directionMouvement = new Vector3(deplacementHorizontal, 0, deplacementVertical).normalized;
                Quaternion rotationVersDirection = Quaternion.LookRotation(directionMouvement, Vector3.up);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotationVersDirection, Time.deltaTime * 10f);
            }
            
        }
        ClientMoveAndRotate();
        ClientVisuals();
    }
}