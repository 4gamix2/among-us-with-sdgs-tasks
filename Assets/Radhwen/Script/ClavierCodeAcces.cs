using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class ClavierCodeAcces : MonoBehaviour
{
    public TextMeshProUGUI affichage;
    public TextMeshProUGUI codeGenereText;
    public Canvas canvasActuel;
    public Canvas nouveauCanvas;  // Ajoutez cette ligne pour le nouveau canvas

    private int codeAcces;  // D�finissez ici la valeur du code d'acc�s fixe

    private void Start()
    {

    }

    public void AjouterChiffre(int chiffre)
    {
        if (affichage.text.Length < 4)
        {
            affichage.text += chiffre.ToString();
        }
    }

    public void Effacer()
    {
        affichage.text = "";
    }

    public void VerifierCodeAcces()
    {

        codeAcces = int.Parse(codeGenereText.text);
        int tentativeCode;
        if (int.TryParse(affichage.text, out tentativeCode))
        {
            if (tentativeCode == codeAcces)
            {
                Debug.Log("Code d'acc�s correct !");

                // D�sactive le canvas actuel
                canvasActuel.gameObject.SetActive(false);

                // Active le nouveau canvas
                nouveauCanvas.gameObject.SetActive(true);

                // Ajoutez ici le code � ex�cuter lorsque le code d'acc�s est correct.
            }
            else
            {
                Debug.Log("Code d'acc�s incorrect !");
            }
        }
        else
        {
            Debug.Log("Entr�e non valide. Veuillez entrer un code � 4 chiffres.");
        }

        Effacer();
    }
}
