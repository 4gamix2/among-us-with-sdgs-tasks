using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ButtonClick : MonoBehaviour
{
    public GameObject panel;
    public VideoPlayer videoPlayer;

    void Start()
    {
        // D�sactive le panneau au d�marrage
        panel.SetActive(false);
    }

    public void OnButtonClick()
    {
        // Active le panneau
        panel.SetActive(true);

        // Charge et lit la vid�o
        videoPlayer.url = "file://" + Application.dataPath + "/video.mp4";
        videoPlayer.Play();
    }
}
