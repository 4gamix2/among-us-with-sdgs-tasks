using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript Instance; // Singleton

    // Vous pouvez assigner le texte dans l'inspecteur Unity
    public Text playerRoleText;

    private void Awake()
    {
        Instance = this; // Initialisation du singleton
    }

    // ... Autres m�thodes et logique du joueur
}

