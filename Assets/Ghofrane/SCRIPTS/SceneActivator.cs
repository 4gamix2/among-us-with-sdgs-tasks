using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneActivator : MonoBehaviour
{
    public string scene; // Le nom de la sc�ne que vous voulez activer

    // M�thode pour activer une autre sc�ne apr�s un d�lai de 2 secondes
    void Start()
    {
        Invoke(nameof(ActivateScene), 2f); // Appelle ActivateScene apr�s un d�lai de 2 secondes
    }

    // M�thode pour activer une autre sc�ne
    void ActivateScene()
    {
        SceneManager.LoadScene(scene);
    }
}
