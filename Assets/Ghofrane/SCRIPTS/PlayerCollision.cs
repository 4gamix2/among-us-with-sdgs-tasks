using UnityEngine;


public class PlayerCollision : MonoBehaviour
{
    public ParticleSystem toxicWasteParticles;
    public AudioSource alarmSound;
    public Light alarmLight;
    public TextManager textManager;
    public AlarmLight alarmLightComponent;
    public GameObject gameOverCanvas; // R�f�rence � votre Canvas de game over
    float timeRemaining = 90f;
    public GameObject activationButton; // R�f�rence au bouton d'activation dans l'interface utilisateur
    public GameObject deactivationButton;  // R�f�rence au bouton de d�sactivation dans l'interface utilisateur
    bool activated = false;
    bool cylinderCollision = false; // Indique si une collision avec le cylindre est en cours
    bool capsuleCollision = false; // Indique si une collision avec la capsule est en cours

    public MemoryInteraction interaction;

    void Start()
    {
        interaction = FindObjectOfType<MemoryInteraction>();

        // D�sactivez le bouton d'activation au d�but
        if (activationButton != null)
            activationButton.gameObject.SetActive(false);

        // D�sactivez le bouton de d�sactivation au d�but
        if (deactivationButton != null)
            deactivationButton.gameObject.SetActive(false);
    }

    void Update()
    {
        if (activated)
        {
            timeRemaining -= Time.deltaTime;
            if (timeRemaining < 0f)
            {
                timeRemaining = 0f;
            }
            textManager.UpdateTimeText(timeRemaining);

            if (timeRemaining <= 0)
            {
                GameOver();
            }
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (!activated && collision.gameObject.CompareTag("Cylinder"))
    //    {
    //        cylinderCollision = true;
    //        // Activez le bouton d'activation lorsque vous entrez en collision avec le cylindre
    //        if (activationButton != null)
    //            activationButton.gameObject.SetActive(true);
    //    }
    //    else if (!activated && collision.gameObject.CompareTag("Capsule"))
    //    {
    //        capsuleCollision = true;
    //        if (deactivationButton != null)
    //            deactivationButton.gameObject.SetActive(true);
    //    }
    //}

    //private void OnCollisionExit(Collision collision)
    //{
    //    if (collision.gameObject.CompareTag("Cylinder"))
    //    {
    //        cylinderCollision = false;
    //        // D�sactivez le bouton d'activation lorsque vous quittez la collision avec le cylindre
    //        if (activationButton != null)
    //            activationButton.gameObject.SetActive(false);
    //    }
    //    else if (collision.gameObject.CompareTag("Capsule"))
    //    {
    //        capsuleCollision = false;
    //        if (deactivationButton != null)
    //            deactivationButton.gameObject.SetActive(false);
    //    }
    //}

    public void activerButtonToxicWaste()
    {
        activationButton.gameObject.SetActive(true);
    }
    public void desactiverButtonToxicWaste()
    {
        activationButton.gameObject.SetActive(false);
    }

    public void ActivateEffects()
    {
        // Activez les effets lorsque vous appuyez sur le bouton d'activation et que vous �tes en collision avec le cylindre
        //if (cylinderCollision)
        //{
            activated = true;
            // D�sactivez le bouton d'activation

            activationButton.gameObject.SetActive(false);
            // Activez le syst�me de particules, le son et la lumi�re

            toxicWasteParticles.Play();

            alarmSound.Play();

            alarmLightComponent.ActivateAlarm();
            textManager.ShowAppearsText();
        //}
    }

    public void DeactivateEffects()
    {
        Debug.Log("Deactivating effects...");
        // D�sactivez les effets lorsque vous appuyez sur le bouton de d�sactivation et que vous �tes en collision avec la capsule

        activated = false;

        deactivationButton.gameObject.SetActive(false);

        toxicWasteParticles.Stop();

        alarmSound.Stop();

        alarmLightComponent.DeactivateAlarm();
        textManager.ShowDisappearText();
        textManager.HideTimeText();
        interaction.memoryGameButton.gameObject.SetActive(false);

    }

    void GameOver()
    {
        Debug.Log("Game Over");
        if (gameOverCanvas != null && timeRemaining <= 0) // V�rifie si le compteur est � z�ro et s'il y a un canvas de game over assign�
        {
            gameOverCanvas.SetActive(true); // Active le canvas de game over
        }
    }
}