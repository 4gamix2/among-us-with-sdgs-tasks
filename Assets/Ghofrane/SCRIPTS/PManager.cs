using Unity.Netcode;
using UnityEngine;

public class PManager : MonoBehaviour
{
    private void OnEnable()
    {
        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnected;
    }

    private void OnDisable()
    {
        if (NetworkManager.Singleton == null) return;

        NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnected;
        NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnected;
    }

    private void OnClientConnected(ulong clientId)
    {
        Debug.Log($"Client connected: {clientId}");

        // Ajoute ici la logique pour g�rer un nouveau joueur connect�
        // Par exemple, ajouter le clientId � une liste de joueurs
    }

    private void OnClientDisconnected(ulong clientId)
    {
        Debug.Log($"Client disconnected: {clientId}");

        // Ajoute ici la logique pour g�rer un joueur d�connect�
        // Par exemple, retirer le clientId de la liste de joueurs
    }
}
