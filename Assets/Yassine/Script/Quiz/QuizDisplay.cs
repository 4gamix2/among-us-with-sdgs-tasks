using UnityEngine;
using UnityEngine.UI;

public class JSONDisplay : MonoBehaviour
{
    public Text textObject; // R�f�rence � l'objet Text dans votre sc�ne
    public TextAsset jsonFile; // Fichier JSON � afficher

    void Start()
    {
        // Charger le contenu du fichier JSON en tant que cha�ne de caract�res
        string jsonContent = LoadJSONFromFile(jsonFile);

        // Formater le JSON pour le rendre plus lisible
        string formattedJSON = FormatJSON(jsonContent);

        // Afficher le contenu format� dans l'objet Text
        textObject.text = formattedJSON;
    }

    string LoadJSONFromFile(TextAsset jsonFile)
    {
        // V�rifier si le fichier JSON est valide
        if (jsonFile == null)
        {
            Debug.LogError("Fichier JSON manquant !");
            return null;
        }

        // Charger le contenu du fichier JSON en tant que cha�ne de caract�res
        string jsonContent = jsonFile.text;
        return jsonContent;
    }

    string FormatJSON(string json)
    {
        // Formater le JSON pour le rendre plus lisible
        // Vous pouvez utiliser des biblioth�ques de traitement JSON ou impl�menter votre propre logique de formatage ici
        // Par exemple, vous pouvez indenter les �l�ments, ajouter des sauts de ligne, etc.
        return json;
    }
}
