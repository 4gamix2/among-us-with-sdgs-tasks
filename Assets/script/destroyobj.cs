using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class destroyobj : MonoBehaviour
{



    public GameObject btnsimo;

    private string nameobjdest;
    public Button button;
    private bool isthis=false;
    public Slider progressBar;

    void Start()
    {
        button.onClick.AddListener(deststone);
    }


    public void deststone()
    {

        Debug.Log("heyyyyyyy");
        if(isthis) {
            if(progressBar.value<0.9f)
            {
               progressBar.value = progressBar.value + 0.34f;
                 Destroy(this.gameObject);
            }
        }
    }

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        isthis = true;
        btnsimo.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void OnTriggerExit(Collider other)
    {
        isthis = false;
        btnsimo.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


    }
}
