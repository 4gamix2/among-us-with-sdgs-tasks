using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking; // For UnityWebRequest
using System.Text; // For Encoding
using UnityEngine;

public class PickupItem : MonoBehaviour
{
    [SerializeField]
    private float pickupRange = 2.6f;

    public PickupBehaviour playerPickupBehaviour;

    [SerializeField]
    private GameObject pickupText;

    [SerializeField]
    private LayerMask layerMask;

    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, pickupRange, layerMask))
        {
            if (hit.transform.CompareTag("Item"))
            {
                pickupText.SetActive(true);

                if (Input.GetKeyDown(KeyCode.E))
                {
                    Item itemComponent = hit.transform.gameObject.GetComponent<Item>();
                    playerPickupBehaviour.DoPickup(itemComponent);
                    StartCoroutine(SendItemToDatabase(itemComponent.itemData));
                }
            }
        }
        else
        {
            pickupText.SetActive(false);
        }
    }

    IEnumerator SendItemToDatabase(ItemData itemData)
    {
        // Prepare the JSON data
        string jsonData = JsonUtility.ToJson(new
        {
            name = itemData.name,
            visual = itemData.visual.ToString(), // This may need to be adjusted based on how you handle visuals
            prefab = itemData.prefab.name
        });

        // Create a new UnityWebRequest, posting JSON to your server
        UnityWebRequest www = new UnityWebRequest("http://localhost:3000/api/inventory/add", "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonData);
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");

        // Send the request and wait for the response
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error while sending: " + www.error);
        }
        else
        {
            Debug.Log("Success: " + www.downloadHandler.text);
        }
    }
}
