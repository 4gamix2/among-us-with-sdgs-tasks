using UnityEngine;
using UnityEngine.UI;

public class RamasserCle : MonoBehaviour
{
    // Assurez-vous de faire r�f�rence au bouton dans l'�diteur Unity en le faisant glisser dans cette variable.
    public GameObject boutonRamassage;
    public GameObject canvas;
    public Button bouton;
    public GameObject objetADetruire;

    private void Start()
    {
        // D�sactivez le bouton de ramassage au d�marrage.
        boutonRamassage.SetActive(false);
        canvas.SetActive(false);
        if (bouton == null)
        {
            bouton = GetComponent<Button>();
        // Ajoutez une fonction � appeler lors du clic sur le bouton
        bouton.onClick.AddListener(OnClick);
        GetComponent<UnityEngine.UI.Button>().onClick.AddListener(OnClick);

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        // V�rifie si le GameObject entrant en collision a un composant de joueur (ajustez le tag en fonction de votre joueur).
        if (other.CompareTag("Player"))
        {
            // Activez le bouton de ramassage lorsque le joueur entre en collision avec la cl�.
            boutonRamassage.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // D�sactivez le bouton de ramassage lorsque le joueur quitte la zone de collision de la cl�.
        if (other.CompareTag("Player"))
        {
            boutonRamassage.SetActive(false);
        }
    }

    public void OnClick()
    {
        // D�sactivez le bouton lorsque vous cliquez dessus
        Destroy(bouton.gameObject);
        canvas.SetActive(true);
        if (objetADetruire != null)
        {
            // D�truisez l'objet associ� lorsque vous cliquez sur le bouton
            Destroy(objetADetruire);
        }
    }
}
