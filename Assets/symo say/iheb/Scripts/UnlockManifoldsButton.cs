using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockManifoldsButton : MonoBehaviour
{
    private int value;
    private Text buttonText;
    private Button button;
    private UnlockManifoldsTask parentTask;




    
    public void Initialize(int value, UnlockManifoldsTask  parentTask)
    {
        if (buttonText == null)
        {
            buttonText = GetComponentInChildren<Text>();
            button = GetComponent<Button>();
            button.onClick.AddListener(OnButtonPressed);
        }

        this.value = value;
        buttonText.text = value.ToString();
        this.parentTask = parentTask;
    }

    public void ToggleButton(bool isOn)
    {
        button.interactable = isOn;
    }
    public void OnButtonPressed()
    {
        parentTask.OnButtonPressed(value,this);
    }


}
