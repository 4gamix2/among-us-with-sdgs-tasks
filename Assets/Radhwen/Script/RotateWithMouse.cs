using UnityEngine;
using UnityEngine.UI;

public class RotateWithMouse : MonoBehaviour
{
    // La vitesse de rotation, peut �tre ajust�e selon les besoins
    public float rotationSpeed = 5f;

    // R�f�rence au slider dans votre interface utilisateur
    public Slider rotationSlider;

    // R�f�rence au Canvas que vous souhaitez fermer
    public Canvas canvasToClose;

    // R�f�rence au syst�me de particules � activer
    public ParticleSystem particleSystemToActivate;

    // Indique si le bouton de la souris est enfonc�
    private bool isMousePressed = false;

    // Valeur du slider avant le maintien du bouton de la souris
    private float initialSliderValue;

    private void Start()
    {
        // Assurez-vous que le slider est configur� correctement
        if (rotationSlider == null)
        {
            Debug.LogError("Slider non assign� dans l'inspecteur.");
        }

        // Assurez-vous que le Canvas est configur� correctement
        if (canvasToClose == null)
        {
            Debug.LogError("Canvas non assign� dans l'inspecteur.");
        }

        // Enregistrez la valeur initiale du slider
        initialSliderValue = rotationSlider.value;
    }

    void Update()
    {
        // Si le bouton de la souris est maintenu enfonc�
        if (Input.GetMouseButtonDown(0))
        {
            isMousePressed = true;

            // R�initialise la valeur du slider au d�but du maintien du bouton
            initialSliderValue = rotationSlider.value;
        }

        // Si le bouton de la souris est rel�ch�
        if (Input.GetMouseButtonUp(0))
        {
            isMousePressed = false;
        }

        // Si le bouton de la souris est maintenu enfonc�, faire pivoter l'objet
        if (isMousePressed)
        {
            // R�cup�rer le mouvement de la souris
            float mouseX = Input.GetAxis("Mouse X");

            // Calculer la rotation en fonction du mouvement de la souris
            float rotationAmount = mouseX * rotationSpeed;

            // Appliquer la rotation autour de l'axe local Z de l'objet
            transform.Rotate(Vector3.forward, rotationAmount);

            // Mettre � jour la valeur du slider en fonction de la rotation
            UpdateSliderValue(rotationAmount);
        }

        // V�rifier si la valeur du slider atteint 100
        if (rotationSlider.value >= 100)
        {
            // Fermer le Canvas (d�sactiver le GameObject du Canvas)
            

            if (particleSystemToActivate != null)
            {
                particleSystemToActivate.gameObject.SetActive(true);
            }

            if (canvasToClose != null)
            {
                Invoke("FermerCanvas", 5f);
            }
        }
    }

    void FermerCanvas()
    {
        // Fermer le Canvas (d�sactiver le GameObject du Canvas)
        if (canvasToClose != null)
        {
            canvasToClose.gameObject.SetActive(false);
        }
    }

    void UpdateSliderValue(float rotationAmount)
    {
        // Augmenter la valeur du slider en fonction de la rotation
        rotationSlider.value = Mathf.Clamp(initialSliderValue + 5f, 0f, 1000f);
    }
}
