using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nextQuiz : MonoBehaviour
{
    public GameObject Quiz1;
    public GameObject Quiz2;
    public GameObject nextQuizPanel;
    public Animator animator;
    public Animator animator1;
    public GameObject otherGameObject;

    void Start()
    {
        Quiz1.SetActive(true);
        Quiz2.SetActive(false);
        nextQuizPanel.SetActive(false);
        Animator animator = otherGameObject.GetComponent<Animator>();
        Animator animator1 = otherGameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    
    public void desactiverInterface1()
    {
        Quiz1.SetActive(false);
        
    }
    public void desactiverInterface2()
    {
        nextQuizPanel.SetActive(true);
        
        Invoke("activerInterface2", 2f);
        
        animator.Play("z");
        


    }
    public void activerInterface1()
    {
        Quiz1.SetActive(true);
    }
    public void activerInterface2()
    {
        Quiz1.SetActive(false);
        Quiz2.SetActive(true);
        animator1.Play("question");
    }
}
