using UnityEngine;

public class DisableCanvas2 : MonoBehaviour
{
    public GameObject canvasParent; // R�f�rence au GameObject parent contenant le Canvas que vous souhaitez d�sactiver

    public void DisableCanvasOnClick()
    {
        // D�sactiver le Canvas
        canvasParent.SetActive(false);
    }
}
