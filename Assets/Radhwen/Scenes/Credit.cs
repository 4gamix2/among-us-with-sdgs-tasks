using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class Credit: MonoBehaviour
{
    // Nom de la sc�ne � charger
    public string sceneName;

    // M�thode pour changer de sc�ne
    public void ChangeScene()
    {
        SceneManager.LoadScene(sceneName);
    }
}
