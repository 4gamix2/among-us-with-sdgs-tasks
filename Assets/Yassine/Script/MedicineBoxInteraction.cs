using UnityEngine;
using UnityEngine.UI;


public class MedicineBoxInteraction : MonoBehaviour
{
    
    public AudioClip medecines; // Effet sonore � jouer

    public GameObject interactionObject; // GameObject pour l'interaction avec la bo�te de m�dicaments
    public bool hasBandage = false; // Indique si le joueur a d�j� pris un bandage
    public RawImage bandageImage;
    public MainMenu mainMenu;



    void Start()
    {
        bandageImage.enabled = false;
        //interactionText.text =interactionCount.ToString();
        mainMenu = FindObjectOfType<MainMenu>();

    }
    void OnTriggerEnter(Collider other)
    {

        AudioSource.PlayClipAtPoint(medecines, transform.position);
        // Si le joueur entre en collision avec l'objet d'interaction et n'a pas d�j� un bandage
        if (other.CompareTag("Player") && !hasBandage && mainMenu.PlayerRole == "Gardien")
        {
            // Activer le GameObject d'interaction pour indiquer que le joueur peut prendre un bandage
            interactionObject.SetActive(true);
            // Activer l'image du bandage dans l'interface pour indiquer qu'un bandage est disponible
            bandageImage.enabled = true;
            // Indiquer que le joueur a maintenant un bandage
            hasBandage = true;
            Debug.Log("Le joueur a pris un bandage");
            
        }

        // Si le joueur entre en collision avec l'objet d'interaction de la tortue et a d�j� un bandage
        else if (other.CompareTag("TurtleInteraction") )
        {
            // Activer le GameObject d'interaction pour indiquer que le joueur peut soigner la tortue
            interactionObject.SetActive(true);
            // D�sactiver l'image du bandage dans l'interface car le joueur l'a utilis� pour soigner la tortue
            bandageImage.enabled = false;
            // Indiquer que le joueur n'a plus de bandage
            hasBandage = false;
            Debug.Log("Le joueur a utilis� un bandage pour soigner la tortue");
        }
    }


}
