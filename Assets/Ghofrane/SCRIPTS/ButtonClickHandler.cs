using UnityEngine;
using UnityEngine.UI;

public class ButtonClickHandler : MonoBehaviour
{
    public GameObject panel; // R�f�rence au panel � activer/d�sactiver
    public GameObject button; // R�f�rence au bouton initial � d�sactiver
    public GameObject closeButton; // R�f�rence au bouton de fermeture du panel

    private void Start()
    {
        // D�sactive le panel et le bouton de fermeture au d�but
        panel.SetActive(false);
        closeButton.SetActive(false);
    }

    // M�thode appel�e lorsque le bouton initial est cliqu�
    public void OnButtonClick()
    {
        // Active ou d�sactive le panel en fonction de son �tat actuel
        panel.SetActive(!panel.activeSelf);

        // Active le bouton de fermeture et d�sactive le bouton initial si le panel est activ�
        closeButton.SetActive(panel.activeSelf);
        button.SetActive(!panel.activeSelf);
    }

    // M�thode appel�e lorsque le bouton de fermeture est cliqu�
    public void OnCloseButtonClick()
    {
        // D�sactive le panel et le bouton de fermeture
        panel.SetActive(false);
        closeButton.SetActive(false);

        // Active le bouton initial
        button.SetActive(true);
    }
}