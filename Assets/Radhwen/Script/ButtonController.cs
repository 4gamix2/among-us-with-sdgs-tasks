﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    
    public Button myButton;  // Référence au bouton dans l'inspecteur
    public MainMenu mainMenu;

    private void Start()
    {
        myButton.gameObject.SetActive(false);  // Désactivez le bouton au départ
        mainMenu = FindObjectOfType<MainMenu>();
    }

    
    
    void OnTriggerEnter(Collider other)
    {
        // V�rifie si le joueur entre en collision avec la sph�re
        if (other.CompareTag("Player")) //&& mainMenu.PlayerRole == "Imposteur")
        {
            myButton.gameObject.SetActive(true); // Active le bouton
        }
    }

    void OnTriggerExit(Collider other)
    {
        // V�rifie si le joueur quitte la collision avec la sph�re
        if (other.CompareTag("Player"))
        {
            myButton.gameObject.SetActive(false); // D�sactive le bouton
        }
    }
}
