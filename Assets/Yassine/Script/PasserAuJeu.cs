using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : NetworkBehaviour
{
    public Text roleText;
    public GameObject interfacePlay;
    private bool impostorAssigned = false;
    public string PlayerRole { get; set; }

    public GameObject imposObject;

    private NetworkVariable<bool> isimpos = new NetworkVariable<bool>(false);

    private NetworkVariable<int> comptegame = new NetworkVariable<int>(0);


    void Update()
    {
        

    }

    public string DeterminePlayerRole()
    {
        if (!impostorAssigned)
        {
            if (Random.value < 0.5f)
            {
                impostorAssigned = true;
                isimpos.Value=true;
                return "Imposteur"; 
            }
        }
        return "Gardien"; 
    }



    public void OnPlayButtonClicked()
    {
       
        interfacePlay.SetActive(false);
        comptegame.Value++;
        if (isimpos.Value == false)
        {
            PlayerRole = DeterminePlayerRole();
        }
        else
        {
            if ((isimpos.Value == false) && (comptegame.Value == 4))
            {
                isimpos.Value = true;
                PlayerRole = "Imposteur";
            }
            else
            {
                PlayerRole = "Gardien";
                imposObject.SetActive(false);
            }
          
          
        }
            Debug.Log(PlayerRole);
            roleText.text = PlayerRole.ToString();
            Invoke("HideRoleText", 10f);
    }

    void HideRoleText()
    {
        // Cache le texte du r�le
        roleText.text = "";
    }
}
