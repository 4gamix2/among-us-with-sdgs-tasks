﻿using UnityEngine;
using UnityEngine.UI;

public class DetectLumiere : MonoBehaviour
{
    public Button LumiereButton; // R�f�rence au bouton du quiz
    public MainMenu mainMenu;
    private void Start()
    {
        LumiereButton.gameObject.SetActive(false); // D�sactive le bouton
        mainMenu = FindObjectOfType<MainMenu>();

    }
    void OnTriggerEnter(Collider other)
    {
        // V�rifie si le joueur entre en collision avec la sph�re
        if (other.CompareTag("Player") && mainMenu.PlayerRole == "Imposteur")
        {
            LumiereButton.gameObject.SetActive(true); // Active le bouton
           
        }
    }

    void OnTriggerExit(Collider other)
    {
        // V�rifie si le joueur quitte la collision avec la sph�re
        if (other.CompareTag("Player"))
        {
            LumiereButton.gameObject.SetActive(false); // D�sactive le bouton
            
        }
    }
}
