using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MultiplayerUI : MonoBehaviour
{
    [SerializeField] Button hostBtn, joinBtn;
   // [SerializeField] GameObject otherCanvas; // Le Canvas � activer

    void Awake()
    {
        AssignInputs();
     
       // otherCanvas.SetActive(false); // D�sactiver l'autre Canvas au d�but
    }

    void AssignInputs()
    {
        hostBtn.onClick.AddListener(delegate { NetworkManager.Singleton.StartHost(); });
        joinBtn.onClick.AddListener(delegate { NetworkManager.Singleton.StartClient(); });
    }

    void ActivateOtherCanvasAndStartHost()
    {
        NetworkManager.Singleton.StartHost();
        SceneManager.LoadScene("SampleScene");
      //  otherCanvas.SetActive(true); // Activer l'autre Canvas
    }

    void ActivateOtherCanvasAndStartClient()
    {
        NetworkManager.Singleton.StartClient();
        // otherCanvas.SetActive(true); // Activer l'autre Canvas
        SceneManager.LoadScene("SampleScene");
    }
}
