using UnityEngine;
using UnityEngine.UI;

public class GestionCanvas : MonoBehaviour
{
    public GameObject objetADetruire;
    public Canvas canvas;

    // Assurez-vous d'attacher cette m�thode � l'�v�nement de fermeture de votre canvas (par exemple, un bouton "Fermer")
    public void FermerCanvas()
    {
        // D�sactive le canvas, vous pouvez ajuster cela selon vos besoins
        canvas.enabled = true;

        // Condition pour d�truire l'objet uniquement lorsque canvas.enabled est �gal � false
        if (!canvas.enabled)
        {
            // Destruction de l'objet
            DetruireObjet(objetADetruire);
        }

        // Autres actions � effectuer lors de la fermeture du canvas
    }

    // Fonction pour d�truire un objet
    private void DetruireObjet(GameObject objet)
    {
        // Code pour d�truire l'objet dans Unity
        Destroy(objet);
    }
}
