using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSwitcher : MonoBehaviour
{
    public Canvas canvas1;
    public Canvas canvas2;

    // Appelez cette fonction lorsque le premier bouton est cliqu�
    public void OnButton1Click()
    {
        canvas1.gameObject.SetActive(true);
        canvas2.gameObject.SetActive(false);
    }

    // Appelez cette fonction lorsque le deuxi�me bouton est cliqu�
    public void OnButton2Click()
    {
        canvas1.gameObject.SetActive(false);
        canvas2.gameObject.SetActive(true);
    }
}