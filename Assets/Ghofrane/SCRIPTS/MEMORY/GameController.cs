using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private Sprite bgImage;
    public Sprite[] puzzles;
    public List<Sprite> gamePuzzles = new List<Sprite>();
    public List<Button> btns =new List<Button>();
    public GameObject canvasToDisable; // R�f�rence au Canvas que vous souhaitez d�sactiver
    private bool firstGuess, secondGuess ;
    private int countGuesses;
    private int countCorrectGuesses;
    private int gameGuesses;
    public GameObject gameFinishedText; // R�f�rence au GameObject contenant le texte de fin de jeu
    private int firstGuessIndex, secondGuessIndex;
    private string firstGuessPuzzle, secondGuessPuzzle;
    public PlayerCollision playerCollision;
    public AudioClip clickAudioClip;

    private AudioSource audioSource;
    private void Awake()
    {
        puzzles = Resources.LoadAll<Sprite>("Sprites/Candy");
        audioSource = GetComponent<AudioSource>();
    }
    void Start()
    {
        playerCollision = FindObjectOfType<PlayerCollision>();
        GetButtons ();
        AddListeners ();
        AddGamePuzzles ();
        Shuffle(gamePuzzles); // Appel � la m�thode Shuffle apr�s avoir ajout� les puzzles
        gameGuesses = gamePuzzles.Count/2;
    }
    void GetButtons()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("PuzzleButton");
        for (int i = 0; i < objects.Length; i++)
        {
            btns.Add(objects[i].GetComponent<Button>());
            btns[i].image.sprite = bgImage;
        }
    }
    void AddGamePuzzles()
    {
        int looper=btns.Count;
        int index = 0;
        for (int i = 0;i < looper;i++)
        {
            if(index == looper/2)
            {
                index = 0;
            }
            gamePuzzles.Add(puzzles[index]);
            index++;
        }
    }
    void AddListeners()
    {
        foreach (Button btn in btns)
        {
            btn.onClick.AddListener(()=>PickAPuzzle());
        }
    }
   
    public void PickAPuzzle()
        {
        PlayAudio();
        if (!firstGuess)
        {firstGuess = true;
            firstGuessIndex= int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
            firstGuessPuzzle = gamePuzzles[firstGuessIndex].name;
            btns[firstGuessIndex].image.sprite = gamePuzzles[firstGuessIndex];
        }else if(!secondGuess)
        {
            secondGuess = true;
            secondGuessIndex = int.Parse(UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name);
            secondGuessPuzzle = gamePuzzles[secondGuessIndex].name;
            btns[secondGuessIndex].image.sprite = gamePuzzles[secondGuessIndex];
            countGuesses++;
            StartCoroutine(CheckIfPuzzlesMatch());
        }
        }
    void PlayAudio()
    {
        audioSource.clip = clickAudioClip;
        audioSource.Play();
    }
    IEnumerator CheckIfPuzzlesMatch()
    {
        yield return new WaitForSeconds(1f);
        if(firstGuessPuzzle == secondGuessPuzzle) {
            yield return new WaitForSeconds(.5f);
            btns[firstGuessIndex].interactable = false;
            btns[secondGuessIndex].interactable = false;
            btns[firstGuessIndex].image.color = new Color(0,0,0,0);
            btns[secondGuessIndex].image.color = new Color(0, 0, 0, 0);
            CheckIfTheGameIsFinished();
        }
        else
        {
            yield return new WaitForSeconds(.5f);
            btns[firstGuessIndex].image.sprite = bgImage;
            btns[secondGuessIndex].image.sprite = bgImage;

        }
        yield return new WaitForSeconds(.5f);
        firstGuess = secondGuess = false;
    }
    IEnumerator ShowGameFinishedText()
    {
        // Active le GameObject contenant le texte de fin de jeu
        gameFinishedText.SetActive(true);

        // Attendre 3 secondes
        yield return new WaitForSeconds(2f);

        // D�sactiver le Canvas
        canvasToDisable.SetActive(false);
    }


    void CheckIfTheGameIsFinished()
    {
        countCorrectGuesses ++;
        if(countCorrectGuesses == gameGuesses)
        {
            Debug.Log("Memory Game Finished");
            Debug.Log("It took you" + countGuesses+"many guesses to finish the game");
            // Afficher le texte de fin de jeu et d�sactiver le Canvas apr�s 3 secondes
            StartCoroutine(ShowGameFinishedText());
            playerCollision.DeactivateEffects();
        }
    }
    void Shuffle(List<Sprite>list)
    {
        for (int i = 0;i<list.Count;i++)
        {
            Sprite sprite = list[i];
            Sprite temp = list[i];
            int randomIndex =Random.Range(i,list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }
  }

  

