using UnityEngine;
using UnityEngine.UI;

public class ImageSwitcher : MonoBehaviour
{
    public Canvas canvas1;
    public Canvas canvas2;

    // Assurez-vous que ce script est attach� � un GameObject avec un bouton
    public Button switchButton;

    void Start()
    {
        // Assurez-vous d'ajouter des listeners aux boutons
        switchButton.onClick.AddListener(SwitchCanvas);
    }

    void SwitchCanvas()
    {
        // D�sactivez le premier canvas et activez le deuxi�me canvas
        canvas1.gameObject.SetActive(false);
        canvas2.gameObject.SetActive(true);
    }
}