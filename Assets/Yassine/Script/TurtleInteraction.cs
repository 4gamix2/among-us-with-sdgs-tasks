using UnityEngine;
using UnityEngine.UI;

public class TurtleInteraction : MonoBehaviour
{
    private int interactionCount = 0;
    public Text interactionText;

    public GameObject medicineBox;
    private RawImage bandageImage;

    void Start()
    {
        interactionText.text = interactionCount.ToString();
        medicineBox = GameObject.Find("MedicineBox");
        bandageImage = medicineBox.GetComponent<MedicineBoxInteraction>().bandageImage;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log(interactionCount);
            if (bandageImage.enabled == true) // Check if bandageImage is assigned
            {
                bandageImage.enabled = false;
                interactionCount++;
                interactionText.text = interactionCount.ToString();
            }
            
        }
    }
}
