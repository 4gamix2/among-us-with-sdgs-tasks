using UnityEngine;

public class EnableCanvas2 : MonoBehaviour
{
    public GameObject canvasParent; // R�f�rence au GameObject parent contenant le Canvas que vous souhaitez activer
    public GameObject button; // R�f�rence au GameObject du bouton que vous souhaitez faire dispara�tre
    public AudioSource clickSound; // R�f�rence � l'AudioSource contenant le son � jouer

    public void EnableCanvasOnClick()
    {
        // Activer le Canvas
        canvasParent.SetActive(true);

        // Faire dispara�tre le bouton
        button.SetActive(false);

        // Jouer le son
        if (clickSound != null)
        {
            clickSound.Play();
        }
    }
}
