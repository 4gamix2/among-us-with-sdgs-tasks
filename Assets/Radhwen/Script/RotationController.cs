using UnityEngine;

public class RotationController : MonoBehaviour
{
    public Transform imageTransform; // R�f�rence � la transform de l'image

    void Update()
    {
        // Met � jour la rotation de l'objet 3D en fonction de la rotation de l'image
        transform.rotation = imageTransform.rotation;
    }
}
