using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Unity.Netcode;
public class netMhosting : MonoBehaviour
{
    public GameObject panelMenu;
    public GameObject panelPlay;
    // Start is called before the first frame update
    [SerializeField] public Button serverbtn;
    [SerializeField] public Button hostbtn;
    [SerializeField] public Button clientbtn;



    private void Start()
    {
        panelPlay.SetActive(false);
    }
    private void Awake()
    {
        serverbtn.onClick.AddListener( () =>
        { NetworkManager.Singleton.StartServer(); });

        hostbtn.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartHost();
            panelMenu.SetActive(false);
            panelPlay.SetActive(true);

        });

        clientbtn.onClick.AddListener( () =>
        {
            NetworkManager.Singleton.StartClient();
            panelMenu.SetActive(false);
            panelPlay.SetActive(true);
        });
    }
}
