using UnityEngine;
using System.Collections;

public class AlarmLight : MonoBehaviour
{
    public Light alarmLight; // R�f�rence � la lumi�re d'alarme
    private bool activated = false; // Pour suivre l'�tat d'activation de l'alarme

    void Start()
    {
        // D�sactiver la lumi�re d'alarme au d�marrage
        alarmLight.enabled = false;
    }

    public void ActivateAlarm()
    {
        if (!activated)
        {
            activated = true;
            // D�marrer le cycle d'alarme
            StartCoroutine(AlarmCycle());
        }
    }

    public void DeactivateAlarm()
    {
        if (activated)
        {
            activated = false;
            // Arr�ter le cycle d'alarme
            StopCoroutine(AlarmCycle());
            // Assurer que la lumi�re est �teinte
            alarmLight.enabled = false;
        }
    }

    IEnumerator AlarmCycle()
    {
        while (activated)
        {
            // Allumer la lumi�re d'alarme
            alarmLight.enabled = true;
            // Attendre pendant 2 secondes
            yield return new WaitForSeconds(2f);
            // �teindre la lumi�re d'alarme
            alarmLight.enabled = false;
            // Attendre pendant 2 secondes
            yield return new WaitForSeconds(2f);
        }
    }
}
