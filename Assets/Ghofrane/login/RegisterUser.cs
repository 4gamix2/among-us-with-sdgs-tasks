using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.UI;

public class RegisterUser : MonoBehaviour
{
    [SerializeField] TMP_InputField nameText;
    [SerializeField] TMP_InputField emailText;
    [SerializeField] TMP_InputField passwordText;
    [SerializeField] TMP_InputField repasswordText;
    [SerializeField] TMP_Text debugText;

    string registerURL = "http://localhost:3000/user/register";

    public void Enter()
    {
        if (nameText.text == "" || emailText.text == "" || passwordText.text == "" || repasswordText.text == "")
        {
            debugText.text = "Field must not be empty";
            return;
        }
        if (passwordText.text != repasswordText.text)
        {
            debugText.text = "Passwords are different!";
            return;
        }

        StartCoroutine(Register(nameText.text, emailText.text, passwordText.text));
    }

    IEnumerator Register(string name, string email, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        form.AddField("email", email);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post(registerURL, form))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
            {
                debugText.text = "Connection error";
                Debug.Log(www.error);
            }
            else
            {
                string responseText = www.downloadHandler.text;

                if (www.responseCode == 200)
                {
                    debugText.text = "User successfully created!";
                }
                else if (www.responseCode == 409)
                {
                    debugText.text = "A user with this email address already exists!";
                }
                else
                {
                    debugText.text = "Unexpected response code: " + www.responseCode;
                }
            }
        }

    }
}
