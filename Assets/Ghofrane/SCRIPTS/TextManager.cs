
using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.UI;

public class TextManager : MonoBehaviour
{
    public TextMeshProUGUI textMeshPro;
    public TextMeshProUGUI timeText;
  

    // M�thode pour masquer le texte du temps
    public void HideTimeText()
    {
        timeText.enabled = false;
    }

    // M�thode pour mettre � jour le texte du temps restant
    public void UpdateTimeText(float time)
    {
        timeText.text = "Time: " + time.ToString("F0");
    }

    // M�thode pour mettre � jour le texte du nombre de vies
   

    // Fonction pour afficher le texte "appears from toxic waste"
    public void ShowAppearsText()
    {
        textMeshPro.text = "Appears from toxic waste";
        textMeshPro.enabled = true;
    }

    // Fonction pour afficher le texte "disappear from toxic waste"
    public void ShowDisappearText()
    {
        textMeshPro.text = "Disappear from toxic waste";
        textMeshPro.enabled = true;
        StartCoroutine(HideTextAfterDelay(5f));
    }

    // Fonction pour masquer le texte
    public void HideText()
    {
        textMeshPro.enabled = false;
    }
    private IEnumerator HideTextAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        textMeshPro.enabled = false;
    }
}