using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    public AudioClip moveSound; // D�finir le clip audio
    private AudioSource audioSource; // R�f�rence au composant AudioSource
    void Start()
    {
        audioSource = GetComponent<AudioSource>(); // Obtenir le composant AudioSource
        audioSource.clip = moveSound; // Attribuer le clip audio au composant AudioSource
    }
    public void Select()
    {
        transform.Find("Text").GetComponent<TextMeshProUGUI>().text = "X";
        GetComponent<Button>().interactable = false;
        int index = int.Parse(gameObject.name);

        TicToc ticTacToe = GameObject.Find("Canvas1").GetComponent<TicToc>();
        ticTacToe.matrix[index] = "X";
        audioSource.Play();

        if (ticTacToe.verification("X"))
        {
            ticTacToe.resultText.text = "You win!"; // Mise � jour du texte pour afficher "X a gagn�"
            StartCoroutine(DeactivateCanvasAfterDelay(3f)); // D�sactiver le canvas apr�s 3 secondes
            Debug.Log("X a gagn�");
        }
        else
        {
            ticTacToe.ComputerPlay();
        }
    }
    IEnumerator DeactivateCanvasAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        GameObject.Find("Canvas1").SetActive(false); // D�sactiver le canvas
    }
}
