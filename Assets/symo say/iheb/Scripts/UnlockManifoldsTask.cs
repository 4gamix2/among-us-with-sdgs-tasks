using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockManifoldsTask : MonoBehaviour
{
    [SerializeField] private List<UnlockManifoldsButton> buttonList = new List<UnlockManifoldsButton>();

    private int currentValue;
    public Slider progressBar;

    private void OnEnable()
    {
        List<int> numbers = new List<int>();

        for (int i = 0; i < buttonList.Count; i++)
        {
            numbers.Add(i + 1);
        }


        for (int i = 0; i < buttonList.Count; i++)
        {
            int pickedNumber = numbers[Random.Range(0, numbers.Count)];
            buttonList[i].Initialize(pickedNumber, this);
            numbers.Remove(pickedNumber);
        }
        currentValue = 1;
    }

    private void ResetButtons()
    {
        foreach (UnlockManifoldsButton button in buttonList)
        {
            button.ToggleButton(true);
        }
    }
     public void OnButtonPressed(int buttonID, UnlockManifoldsButton currentButton)
     {
         if (currentValue >= buttonList.Count)
         {
             Debug.Log("[unlock succeeded]");
             gameObject.SetActive(false);
            progressBar.value = 1f;
        }
         if (currentValue == buttonID)
         {
             currentValue++;
             currentButton.ToggleButton(false);
         }
         else
         {
             currentValue = 1;
             ResetButtons();
             Debug.Log("[unlock failed]");
         }
      
     }

    
}
