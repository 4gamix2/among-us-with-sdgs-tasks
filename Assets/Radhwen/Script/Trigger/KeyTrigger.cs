﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyTrigger : MonoBehaviour
{
    public Button KeyButton;
    // Start is called before the first frame update
    public MainMenu mainMenu;
    public void start()
    {
        mainMenu = FindObjectOfType<MainMenu>();
        KeyButton.gameObject.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        // V�rifie si le joueur entre en collision avec la sph�re
        if (other.CompareTag("Player") && mainMenu.PlayerRole == "Gardien") //&& mainMenu.PlayerRole == "Imposteur")
        {
            KeyButton.gameObject.SetActive(true); // Active le bouton
        }
    }

    void OnTriggerExit(Collider other)
    {
        // V�rifie si le joueur quitte la collision avec la sph�re
        if (other.CompareTag("Player"))
        {
            KeyButton.gameObject.SetActive(false); // D�sactive le bouton
        }
    }
}