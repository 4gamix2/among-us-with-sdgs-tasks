using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class OpenAndCloseUI : MonoBehaviour
{
    public GameObject GameUi;
    // Start is called before the first frame update

    public void OpenAndCloseV(){
        if(GameUi.activeSelf){
            GameUi.SetActive(false);

        }
        else {
            GameUi.SetActive(true);
        }
    }

}
